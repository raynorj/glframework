#pragma once
#ifndef _CORE_INCLUDES_H_
#define _CORE_INCLUDES_H_

#include <stdio.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <map>
#include <set>
#include <experimental/filesystem>
#include <filesystem>
#include <memory>
//#include "sol.hpp"
#include "json.hpp"
#include <regex>
#include "Config.h"

typedef const char* cchar;
typedef unsigned char uchar;
typedef int s32;
typedef unsigned int u32;
typedef float f32;
typedef double f64;
typedef std::string string;
using namespace std::experimental::filesystem;
typedef nlohmann::json JSON;

#endif