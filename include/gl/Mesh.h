#pragma once
#ifndef _MESH_H_
#define _MESH_H_

#include "MeshPart.h"
#include <vector>

/**!
 * A model, or mesh, contains a collection of grouped submeshes.
 * Each submesh has it's own vertex and index buffers, and for now it's own VAO.
 * Materials are defined per submesh, rather than per mesh.
 */
class Mesh : public Entity
{
public:
	Mesh();
	~Mesh();

	void AddPart(MeshPart* part);
	u32 GetPartCount();
	MeshPart* GetPart(u32 index);

private:
	std::vector<MeshPart*> m_Parts;
};
typedef std::shared_ptr<Mesh> MeshPtr;
#endif