#pragma once
#ifndef _SPOT_LIGHT_H_
#define _SPOT_LIGHT_H_

#include "Light.h"

struct SpotLightData
{
	vec3 Position = vec3(0.0f);
	vec3 Direction = vec3(0.0f, 1.0f, 0.0f);
	vec3 Color = vec3(1.0f);
	f32 Intensity = 1000.0f;
	f32 Radius = 100.0f;
	f32 ConeAngle = 30.0f;
};

class SpotLight : public Light
{
public:
	SpotLight();
	~SpotLight();

	void SetPosition(vec3 pos);
	vec3 GetPosition();

	void SetDirection(vec3 dir);
	vec3 GetDirection();

	void SetRadius(f32 r);
	f32 GetRadius();

	void SetConeAngle(f32 angle);
	f32 GetConeAngle();

	SpotLightData GetData();

	Camera* GetCamera();
private:
	vec3 m_Position;
	vec3 m_Direction;
	f32 m_Radius;
	f32 m_ConeAngle;

	SpotLightData m_Data;
	Camera* m_Camera;
};
#endif