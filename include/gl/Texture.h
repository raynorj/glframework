#pragma once
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include "CoreIncludes.h"
#include "GLDefines.h"
#include "TextureTypes.h"



class Texture
{
public:
	Texture();
	~Texture();
	void Create(TextureType type, TextureFormat format, InternalFormat internal_format, TextureData data_type, s32 width, s32 height, s32 depth = 1, s32 levels = 1);
	
	void Destroy();

	s32 GetWidth();
	s32 GetHeight();
	s32 GetDepth();

	TextureType GetType();
	TextureFormat GetFormat();
	InternalFormat GetInternalFormat();
	TextureData GetDataType();

	u32 GetHandle();

	void* GetData();
	void GenerateMipMaps();

	void SetData(void* data);
	void SetFaceData(u32 face, void* data);

	void SetMinFilter(FilterMode filter);
	void SetMagFilter(FilterMode filter);

	void SetWrapS(WrapMode wrap);
	void SetWrapT(WrapMode wrap);
	void SetWrapR(WrapMode wrap);

	void SetBaseMipLevel(u32 level);
	void SetMaxMipLevel(u32 level);

	void SetCompareMode(CompareMode mode);
	void SetCompareFunc(CompFunc func);
	bool operator == (Texture* t)
	{
		return t != nullptr && GetHandle() == t->GetHandle();
	}

private:
	TextureType m_Type;
	TextureFormat m_Format;
	InternalFormat m_InternalFormat;
	TextureData m_DataType;
	s32 m_Width;
	s32 m_Height;
	s32 m_Depth;
	u32 m_TextureID;
	
};
typedef std::shared_ptr<Texture> TexturePtr;
#endif