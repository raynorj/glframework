#pragma once
#ifndef _ORTHO_CAMERA_H_
#define _ORTHO_CAMERA_H_

#include "Camera.h"

class OrthoCamera : public Camera
{
public:
	void Update() override;

	void SetExtents(vec4 ext);

private:
	vec4 m_Extents;
};
#endif