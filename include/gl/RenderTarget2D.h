#pragma once
#ifndef _RENDER_TARGET2D_H_
#define _RENDER_TARGET2D_H_
#include "Texture.h"
#include "Framebuffer.h"

class RenderTarget2D
{
public:
	RenderTarget2D();
	~RenderTarget2D();

	void Create(TexturePtr tex, FramebufferPtr fbo);
	void Destroy();
	u32 GetTextureID();
	FramebufferPtr GetFBO();

	TexturePtr GetTexturePtr();
private:
	TexturePtr m_Texture;
	FramebufferPtr m_FBO;
};
#endif
