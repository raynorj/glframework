#pragma once
#ifndef _FULL_SCREEN_QUAD_H_
#define _FULL_SCREEN_QUAD_H_

#include "GLDefines.h"
#include "Mesh.h"
#include "VertexArray.h"

class FullScreenQuad : public Mesh
{
public:
	FullScreenQuad();
	~FullScreenQuad();

	void Create(VertexArray* VAO);

	VertexArray* GetVAO();
	u32 GetIndicies();
	
private:
};
#endif