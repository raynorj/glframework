#pragma once
#ifndef _SHADER_H_
#define _SHADER_H_

#include "CoreIncludes.h"
#include "GLDefines.h"


class Shader
{
public:
	Shader(u32 handle, ShaderType type, ShaderBits bit);
	~Shader();
	void SetHandle(u32 shader);
	u32 GetHandle();

	ShaderType GetType();
	ShaderBits GetStageBit();

	void SetUniform(s32 location, f32 value);
	void SetUniform(s32 location, vec2 value);
	void SetUniform(s32 location, vec3 value);
	void SetUniform(s32 location, vec4 value);
	void SetUniform(s32 location, mat4 value);
	void SetUniform(s32 location, s32 value);
	void SetUniform(s32 location, u32 value);

	void SetUniformByName(string name, f32 value);
	void SetUniformByName(string name, vec2 value);
	void SetUniformByName(string name, vec3 value);
	void SetUniformByName(string name, vec4 value);
	void SetUniformByName(string name, mat4 value);
	void SetUniformByName(string name, s32 value);
	void SetUniformByName(string name, u32 value);
	



	void Dispatch(u32 size_x, u32 size_y, u32 size_z);

	void ClearUniforms();

private:
	u32 m_ShaderProgram;
	ShaderType m_Type;
	ShaderBits m_StageBit;
	std::map<string, s32> m_UniformLocations;

	s32 GetUniformLocation(string name);
};
typedef std::shared_ptr<Shader> ShaderPtr;
#endif