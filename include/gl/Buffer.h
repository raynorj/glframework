#pragma once
#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "CoreIncludes.h"
#include "BufferTypes.h"

/*! \class Buffer
 * \brief Simple class that encapsulates a generic OpenGL buffer
 */
class Buffer
{
public:
	Buffer();
	~Buffer();

	void Create(BufferType type, BufferAccess access_flag, BufferUsageBit usage);
	void Delete();

	void SetData(void* data, u32 size, u32 el_size, u32 type_size);
	void* GetData();
	u32 GetSize();
	void SetElementSize(u32 size);
	u32 GetElementSize();
	u32 GetTypeSize();
	u32 GetHandle();
	BufferType GetType();

private:
	u32 m_BufferID;
	u32 m_Size;
	u32 m_ElementSize;
	u32 m_TypeSize;
	BufferType m_Type;
	BufferAccess m_Access;
	BufferUsageBit m_Usage;
	bool m_IsMapped;
};
#endif