#ifndef _PING_PONG_RENDER_TARGET_H_
#define _PING_PONG_RENDER_TARGET_H_

#include "RenderTarget2D.h"

class PingPongRenderTarget2D
{
public:

	void Swap();

private:
	RenderTarget2D* m_Ping;
	RenderTarget2D* m_Pong;
};
#endif