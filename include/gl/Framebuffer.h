#pragma once
#ifndef _FRAMEBUFFER_H_
#define _FRAMEBUFFER_H_
#include "CoreIncludes.h"
#include "GLDefines.h"
#include "Texture.h"

class Framebuffer
{
public:
	Framebuffer();
	~Framebuffer();

	void Create(u32 width, u32 height);
	void Destroy();

	void Finish();

	void Bind();
	void Unbind();

	void Clear(vec4 color);
	void BeginDraw();

	void BindTexture(u32 slot, TexturePtr texture);
private:
	u32 m_BufferID;

	std::vector<TexturePtr> m_Targets;
	std::vector<u32> m_Binds;
	u32 m_Width;
	u32 m_Height;
};


typedef std::shared_ptr<Framebuffer> FramebufferPtr;
#endif