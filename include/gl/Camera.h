#pragma once
#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "CoreIncludes.h"
#include "Entity.h"

class Camera : public Entity
{
public:
	Camera();
	~Camera();

	void Update() override;

	void SetAspectRatio(f32 ratio);
	void SetNearPlane(f32 near);
	void SetFarPlane(f32 far);
	void SetFOV(f32 fov);
	void SetDirection(vec3 dir);

	f32 GetAspectRatio();
	f32 GetNearPlane();
	f32 GetFarPlane();
	f32 GetFOV();
	vec3 GetDirection();

	mat4 GetViewMatrix();
	virtual mat4 GetProjectionMatrix();

protected:
	mat4 m_ViewMatrix;
	mat4 m_ProjectionMatrix;
	vec3 m_Direction;
	f32 m_AspectRatio;
	f32 m_NearPlane;
	f32 m_FarPlane;
	f32 m_FOV;
};

#endif