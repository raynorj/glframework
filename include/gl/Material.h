#pragma once
#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include "Texture.h"
class Material
{
public:
	Material(u32 id);
	~Material();

	u32 GetID();

	void SetUseDiffuseMap(bool use);
	bool GetUseDiffuseMap();

	void SetDiffuseMap(TexturePtr map);
	TexturePtr GetDiffuseMap();

	void SetDiffuse(vec3 color);
	vec3 GetDiffuse();


	void SetUseOpacityMap(bool use);
	bool GetUseOpacityMap();

	void SetOpacityMap(TexturePtr map);
	TexturePtr GetOpacityMap();

	void SetOpacity(f32 opacity);
	f32 GetOpacity();


	void SetUseNormalMap(bool use);
	bool GetUseNormalMap();

	void SetNormalMap(TexturePtr map);
	TexturePtr GetNormalMap();


	void SetUseRoughnessMap(bool use);
	bool GetUseRoughnessMap();

	void SetRoughnessMap(TexturePtr map);
	TexturePtr GetRoughnessMap();

	void SetRoughness(f32 rough);
	f32 GetRoughness();


	void SetUseMetalnessMap(bool use);
	bool GetUseMetalnessMap();

	void SetMetalnessMap(TexturePtr map);
	TexturePtr GetMetalnessMap();

	void SetMetalness(f32 metal);
	f32 GetMetalness();

private:

	u32 m_ID;

	TexturePtr m_DiffuseMap = nullptr;
	TexturePtr m_OpacityMap = nullptr;
	TexturePtr m_NormalMap = nullptr;
	TexturePtr m_RoughnessMap = nullptr;
	TexturePtr m_MetalnessMap = nullptr;

	bool m_UseDiffuseMap;
	bool m_UseOpacityMap;
	bool m_UseNormalMap;
	bool m_UseRoughnessMap;
	bool m_UseMetalnessMap;

	vec3 m_DiffuseColor;
	f32 m_Opacity;
	f32 m_Roughness;
	f32 m_Metalness;
};
#endif