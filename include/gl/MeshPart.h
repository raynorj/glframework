#pragma once
#ifndef _MESH_PART_H_
#define _MESH_PART_H_

#include "GLDefines.h"
#include "VertexArray.h"
#include "Material.h"

#include "Entity.h"

class MeshPart : public Entity
{
public:
	MeshPart();
	~MeshPart();

	void SetData(VertexArray* vao, Buffer* ibo);
	void SetMaterial(Material* mat);
	Material* GetMaterial();

	VertexArray* GetVertexArray();
private:
	VertexArray* m_VAO;
	Buffer* m_IndexBuffer;
	Material* m_Material;
};
#endif