#pragma once
#ifndef _RENDER_PASS_H_
#define _RENDER_PASS_H_

#include "RenderTarget2D.h"
#include "Shader.h"

class RenderPass
{
public:
	RenderPass();
	~RenderPass();

	void AddInput(TexturePtr in);
	void SetOutput(FramebufferPtr out);
	void SetShaderVS(ShaderPtr shader);
	void SetShaderPS(ShaderPtr shader);
	
	std::vector<TexturePtr> GetInput();
	FramebufferPtr GetOutput();
	ShaderPtr GetShaderVS();
	ShaderPtr GetShaderPS();
private:
	std::vector<TexturePtr> m_Input;
	FramebufferPtr m_Output;
	ShaderPtr m_ShaderVS;
	ShaderPtr m_ShaderPS;
};
#endif