#pragma once
#ifndef _POINT_LIGHT_H_
#define _POINT_LIGHT_H_

#include "Light.h"

struct PointLightData
{
	vec3 Position = vec3(0.0f);
	vec3 Color = vec3(1.0f);
	f32 Intensity = 1000.0f;
	f32 Radius = 100.0f;
};

class PointLight : public Light
{
public:
	PointLight();
	~PointLight();

	void SetPosition(vec3 pos);
	vec3 GetPosition();

	void SetRadius(f32 r);
	float GetRadius();

	PointLightData GetData();
	Camera* GetCamera();
private:
	vec3 m_Position;
	f32 m_Radius;
	Camera* m_Camera;
	PointLightData m_Data;
};
#endif