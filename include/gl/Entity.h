#pragma once
#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "GLDefines.h"
#include "CoreIncludes.h"

struct AABB
{
	vec3 MinPoint;
	vec3 MaxPoint;
};
class Entity
{
public:
	Entity();
	~Entity();

	virtual void Update();

	void SetPosition(vec3 pos);
	void SetX(f32 x);
	void SetY(f32 y);
	void SetZ(f32 z);

	vec3 GetPosition();
	f32 GetX();
	f32 GetY();
	f32 GetZ();

	void SetRotation(vec3 rot);
	void SetYaw(f32 x);
	void SetPitch(f32 y);
	void SetRoll(f32 z);

	vec3 GetRotation();
	f32 GetYaw();
	f32 GetPitch();
	f32 GetRoll();

	void SetScale(vec3 scale);
	void SetScaleX(f32 x);
	void SetScaleY(f32 y);
	void SetScaleZ(f32 z);

	vec3 GetScale();
	f32 GetScaleX();
	f32 GetScaleY();
	f32 GetScaleZ();

	mat4 GetModelMatrix();

	AABB GetAABB();


protected:
	vec3 m_Position;
	vec3 m_Rotation;
	vec3 m_Scale;
	AABB m_AABB;
	AABB m_TransformedAABB;

	mat4 m_ModelMatrix;
	bool m_Dirty;
};
#endif