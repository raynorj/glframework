#pragma once
#ifndef _VERTEX_ARRAY_H_
#define _VERTEX_ARRAY_H_

#include "CoreIncludes.h"
#include "GLDefines.h"
#include "Buffer.h"

class VertexArray
{
public:
	VertexArray();
	~VertexArray();
	void Create();
	void Destroy();
	void BindBuffer(Buffer* buffer, u32 binding);
	u32 GetSize();
	u32 GetHandle();


private:
	u32 m_ArrayID;
	Buffer* m_IndexBuffer;
	bool m_IsBound;
};
#endif