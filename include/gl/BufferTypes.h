#pragma once
#ifndef _BUFFER_TYPES_H_
#define _BUFFER_TYPES_H_
#include <gl\glew.h>

/*! \enum BufferType
 *  \brief enum for identification of various OpenGL buffers
 */
enum BufferType
{
	BT_VERTEX = GL_ARRAY_BUFFER,
	BT_INDEX = GL_ELEMENT_ARRAY_BUFFER,
	BT_TRANSFORM_FEEDBACK = GL_TRANSFORM_FEEDBACK_BUFFER,
	BT_UNIFORM = GL_UNIFORM_BUFFER,
	BT_ATOMIC_COUNTER = GL_ATOMIC_COUNTER_BUFFER,
	BT_SHADER_STORAGE = GL_SHADER_STORAGE_BUFFER,
	BT_TEXTURE = GL_TEXTURE_BUFFER,

	BT_UNKNOWN = -1
};

/*! \enum BufferUsage
 *  \brief enum for various OpenGL buffer useage flags
 */
enum BufferUsage
{
	BU_STATIC_DRAW = GL_STATIC_DRAW,
	BU_STATIC_READ = GL_STATIC_READ,
	BU_STATIC_COPY = GL_STATIC_COPY,

	BU_STREAM_DRAW = GL_STREAM_DRAW,
	BU_STREAM_READ = GL_STREAM_READ,
	BU_STREAM_COPY = GL_STREAM_COPY,

	BU_DYNAMIC_DRAW = GL_DYNAMIC_DRAW,
	BU_DYNAMIC_READ = GL_DYNAMIC_READ,
	BU_DYNAMIC_COPY = GL_DYNAMIC_COPY
};

/*! \enum BufferAccess
 *  \brief enum for OpenGL buffer access flags
 */
enum BufferAccess
{
	BA_READ = GL_READ_ONLY,
	BA_WRITE = GL_WRITE_ONLY,
	BA_RW = GL_READ_WRITE
};

enum BufferUsageBit
{
	BU_DYNAMIC = GL_DYNAMIC_STORAGE_BIT,
	BU_READABLE = GL_MAP_READ_BIT,
	BU_WRITABLE = GL_MAP_WRITE_BIT,
	BU_RWABLE = GL_MAP_READ_BIT | GL_MAP_WRITE_BIT,
	BU_NONE = 0
};
/*! \struct BufferFlags
 * \brief stores flags for buffer creation
 */
struct BufferFlags
{
	BufferType TypeFlag;
	BufferAccess AccessFlag;
	BufferUsage UsageFlag;
	BufferUsageBit UsageBit;
};
#endif