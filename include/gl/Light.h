#pragma once
#ifndef _LIGHT_H_
#define _LIGHT_H_

#include "CoreIncludes.h"
#include "GLDefines.h"

#include "Camera.h"

class Light
{
public:
	Light();
	~Light();

	void SetColor(vec3 color);
	vec3 GetColor();

	void SetIntensity(f32 i);
	float GetIntensity();

	void SetCastsShadow(bool cast);
	bool GetCastsShadow();

private:
	vec3 m_Color;
	f32 m_Intensity;

	bool m_CastsShadow;
};
#endif