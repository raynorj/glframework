#pragma once
#ifndef _GL_RENDERER_H
#define _GL_RENDERER_H_

#include <SDL.h>
#include "Remotery.h"

#include "CoreIncludes.h"
#include "GLDefines.h"

#include "Buffer.h"
#include "Camera.h"
#include "OrthoCamera.h"
#include "Mesh.h"
#include "Material.h"
#include "Shader.h"
#include "Texture.h"
#include "Framebuffer.h"
#include "RenderTarget2D.h"
#include "RenderPass.h"
#include "FullscreenQuad.h"


struct RendererOptions
{
	u32 WindowWidth;
	u32 WindowHeight;
	ivec2 WindowSize;
	vec3 ClearColor;
	string WindowTitle;
	bool VSync;
};

struct ContextState
{
	bool DepthTest;
	bool DepthWrite;
	bool ColorWrite;
	bool VSync;

	u32 ScreenMode;

	PolygonMode FrontMode;
	PolygonMode BackMode;

	PolygonFace CullFace;

	FaceWinding Winding;

	BlendFunc SrcFunc;
	BlendFunc DstFunc;
	BlendFunc BlendFunc;

	BlendEquation BlendEq;

	CompFunc DepthFunc;

	std::map<Operation, bool> OpState;

	std::array<u32, 8> TextureUnits;
	Material* CurrentMaterial;

};

struct GLMultiDrawElementsCommand
{

};

struct DrawElementsIndirectCommand
{
	u32  VertexCount;
	u32  InstanceCount;
	u32  FirstIndex;
	u32  BaseVertex;
	u32  BaseInstance;
};

typedef std::vector<DrawElementsIndirectCommand> DrawElementsIndirectCommandList;
static void GLAPIENTRY DebugCallback(u32 source, u32 type, u32 id, u32 severity, s32 length, cchar message, const void* userParam);

class GLRenderer
{
public:

	bool Init(RendererOptions opts);
	void Destroy();

	void SetWindowTitle(string title);
	void SetWindowFullscreenMode(u32 mode);
	void SetWindowVSync(bool vsync);
	void SetWindowsSize(ivec2 size);

	void BeginFrame();
	void EndFrame();

	void DrawArrays(VertexArray* array, PrimitiveType type);
	void DrawElements(u32 count, PrimitiveType type);


	void SetFaceWinding(FaceWinding winding);
	void SetCullFace(PolygonFace face);

	void Enable(Operation op);
	void Disable(Operation op);

	void Clear(BufferBits bits);
	void Clear(TexturePtr texture, void* data);

	void SetBlendFunc(BlendFunc fSrc, BlendFunc fDst);
	void SetBlendFuncSeparate();
	void SetBlendEquation(BlendEquation eq);

	void SetDepthFunc(CompFunc func);
	void SetDepthWrite(bool enable);

	void SetPolygonMode(PolygonFace face, PolygonMode mode);

	void Bind(VertexArray* vao);
	void Bind(u32 unit, TexturePtr texture);
	void BindImage(u32 unit, TexturePtr texture, BufferAccess access);
	void BindImageLayer(u32 unit, TexturePtr texture, s32 layer, BufferAccess access);
	
	void SetActiveShader(ShaderPtr shader);
	void ResetShader(ShaderBits bit);

	void SetActiveFramebuffer(FramebufferPtr frame_buf = nullptr);
	void SetActiveRenderPass(RenderPass* pass);

	FramebufferPtr CreateFramebuffer(ivec2 size, std::vector<TexturePtr> textures);

	TexturePtr CreateRenderTargetTexture(ivec2 size, TextureFormat format, vec4 clear_color = vec4(0.0f));
	RenderTarget2D* CreateRenderTarget2D(ivec2 size, TextureFormat format, vec4 clear_color = vec4(0.0f));

	TexturePtr CreateRenderTargetTexture(ivec3 size, TextureFormat format, s32 levels = 1, vec4 clear_color = vec4(0.0f));
	RenderPass* CreateRenderPass();
	
	RendererOptions GetOptions();

	void AddMaterial(Material* m);
	std::vector<Material*> GetMaterials();

	void RenderFSQuad();

	void SetQuadVS(ShaderPtr shader);
	void SetQuadPS(ShaderPtr shader);

	ShaderPtr GetQuadPS();
private:
	SDL_Window* m_Window;
	SDL_GLContext m_Context;
	RendererOptions m_Opts;
	ContextState m_State;
	u32 m_ProgramPipeline;

	std::vector<Material*> m_Materials;

	FullScreenQuad* m_Quad;
	ShaderPtr m_QuadVS;
	ShaderPtr m_QuadPS;
};
#endif