#pragma once
#ifndef _GL_APP_H_
#define _GL_APP_H_

#include <chrono>
using namespace std::chrono;

#include "GLRenderer.h"
#include "InputHandler.h"
#include "ResourceLoader.h"
//#include "ScriptManager.h"
#include "Camera.h"

#include "RenderPass.h"
#include "Config.h"

#include "Remotery.h"

struct CameraMatrices
{
	mat4 M;
	mat4 V;
	mat4 P;
	mat4 MV;
	mat4 MVP;
};

class GLApp
{
public:
	virtual void Init(string root_dir);

	virtual void Destroy();

	virtual void Run();

	virtual void RenderFrame();

	void AddRenderPass(RenderPass* pass);

	GLRenderer* GetRenderer();
	InputHandler* GetInput();
	ResourceLoader* GetLoader();
	Camera* GetCamera();
	TwBar* GetUIBar();
	f32 GetDeltaTime();
protected:

	GLRenderer* m_Renderer;
	InputHandler* m_Input;
	ResourceLoader* m_Loader;
	Camera* m_Camera;
	CameraMatrices m_CameraMatrices;

	TwBar* m_UI;
	bool m_ShowUI;

	MeshPtr m_Scene;

	vec3 cam_pos;
	vec3 cam_dir;
	f32 cam_move_speed;
	f32 cam_look_speed;
	
	int frames;
	Remotery* m_Remote;
private:

	

	f32 cam_yaw;
	f32 cam_pitch;

	float fps;
	float frame_time;
	float dt;
	
	time_point<high_resolution_clock> now;
	time_point<high_resolution_clock> then;
	duration<double> time;
	duration<float> rec;
	float time_seconds;
	double delay;
	double delay_count;

};
#endif