#pragma once
#ifndef _RESOURCELOADER_H_
#define _RESOURCELOADER_H_

#include <time.h>
#include <SDL.h>

#include "Importer.hpp"
#include "scene.h"
#include "postprocess.h"

#include "CoreIncludes.h"
#include "GLDefines.h"
#include "GLRenderer.h"


class ResourceLoader
{
public:
	ResourceLoader(cchar root, GLRenderer* renderer);
	~ResourceLoader();
	void UpdateResources();

	void SetRootDirectory(path dir);
	path GetRootDirectory();

	void SetRenderer(GLRenderer* renderer);
	GLRenderer* GetRenderer();

	ShaderPtr LoadShader(path filename);
	MeshPtr LoadMesh(path filename);
	TexturePtr LoadTexture(path filename);
	JSON LoadJSON(path filename);
	Material* LoadMaterial(path filename);
private:
	path m_RootDir;
	path m_ExeDir;
	GLRenderer* m_Renderer;
	std::map<path, file_time_type> m_ResourceWriteTime;
	std::map<path, bool> m_ShouldReload;
	std::map<path, ShaderPtr> m_ShaderCache;
	std::map<path, MeshPtr> m_MeshCache;
	std::map<path, TexturePtr> m_TextureCache;
	std::map<path, Material*> m_MaterialCache;
	std::map<path, JSON> m_JSONCache;

	path GetResourcePath(path file);

	path GetAssimpTexture(aiMaterial* imp_mat, aiTextureType type, path dir, Material* mat);

	bool IsDuplicate(TexturePtr t1, TexturePtr t2);
};
#endif