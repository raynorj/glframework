#pragma once
#ifndef  _INPUT_HANDLER_H_
#define _INPUT_HANDLER_H_

#include "CoreIncludes.h"
#include <SDL.h>
#include <map>
#include "AntTweakBar.h"
enum MouseButton
{
	MB_LEFT = SDL_BUTTON_LEFT,
	MB_RIGHT = SDL_BUTTON_RIGHT,
	MB_MIDDLE = SDL_BUTTON_MIDDLE
};

class InputHandler
{
public:
	InputHandler();
	~InputHandler();

	void SetMouseCapture(bool enabled);
	void Update();

	bool IsKeyDown(SDL_Keycode key);
	bool IsKeyUp(SDL_Keycode key);
	bool IsKeyReleased(SDL_Keycode key);
	bool IsKeyJustReleased(SDL_Keycode key);
	bool IsKeyPressed(SDL_Keycode key);
	bool IsKeyJustPressed(SDL_Keycode key);

	bool IsMouseButtonDown(MouseButton button);
	bool IsMouseButtonUp(MouseButton button);
	bool IsMouseButtonReleased(MouseButton button);
	bool IsMouseButtonJustReleased(MouseButton button);
	bool IsMouseButtonPressed(MouseButton button);
	bool IsMouseButtonJustPressed(MouseButton button);

	f32 GetRelativeMouseX();
	f32 GetRelativeMouseY();
	s32 GetMouseX();
	s32 GetMouseY();
	bool IsMouseCaptured();
	

	bool ShouldQuit();

private:
	std::map<int, bool> m_KeyState;
	std::map<int, bool> m_LastKeyState;
	std::map<int, bool> m_MouseButtonState;
	std::map<int, bool> m_LastMouseButtonState;
	s32 m_MouseX;
	s32 m_MouseY;
	s32 m_RelativeMouseX;
	s32 m_RelativeMouseY;
	s32 m_PrevRelativeMouseX;
	s32 m_PrevRelativeMouseY;
	bool m_Quit;
	bool m_MouseCapture;
};
#endif
