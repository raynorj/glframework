#include "GLApp.h"

void TW_CALL StrCopy(std::string& dest, const std::string& src)
{
	dest = src;
}

void GLApp::Init(string root_dir)
{
	time = std::chrono::duration<double>(0.0);
	rec = std::chrono::duration<float>(0.0f);
	delay = 0.0;
	time_seconds = 0.0f;
	delay_count = 0.0;
	frames = 0;

	cam_move_speed = 1.0f;
	cam_look_speed = 0.25f;
	cam_yaw = -90.0f;
	cam_pitch = 0.0f;

	fps = 0.0f;
	frame_time = 0.0f;

	now = high_resolution_clock::now();
	then = now;

	m_Loader = new ResourceLoader(root_dir.c_str(), nullptr);
	//m_Script = new ScriptManager(m_Loader);
	//m_Script->LoadScript("config.lua");

	RendererOptions opts;
	opts.ClearColor = vec3(0.0f, 0.0f, 0.0f);

	//opts.WindowHeight = m_Script->State.get<u32>("Height");
	//opts.WindowWidth = m_Script->State.get<u32>("Width");
	//opts.WindowTitle = m_Script->State.get<u32>("Title");
	//opts.VSync = m_Script->State.get<bool>("VSync");

	opts.WindowHeight = 720;
	opts.WindowWidth = 1280;
	opts.WindowTitle = "GL Test";
	opts.VSync = false;
	m_Renderer = new GLRenderer();
	m_Renderer->Init(opts);
	
	m_Input = new InputHandler();
	m_Loader->SetRenderer(m_Renderer);

	m_Renderer->SetQuadVS(m_Loader->LoadShader("shaders/NDC_Passthrough.vs"));
	m_Renderer->SetQuadPS(m_Loader->LoadShader("shaders/Full_Screen_Quad.ps"));

	// Camera set up
	m_Camera = new Camera();
	m_Camera->SetAspectRatio((float)opts.WindowWidth / (float)opts.WindowHeight);
	m_Camera->SetFOV(glm::radians(70.0f));
	m_Camera->SetNearPlane(0.1f);
	m_Camera->SetFarPlane(1000.0f);
	m_Camera->SetPosition(vec3(2.0f, 10.0f, 2.0f));
	m_Camera->SetDirection(vec3(-10.0f, 0.0f, 0.0f));
	m_Camera->Update();

	// UI setup
	TwInit(TW_OPENGL_CORE, NULL);
	TwWindowSize(opts.WindowWidth, opts.WindowHeight);

	m_UI = TwNewBar("Settings");
	m_ShowUI = true;

	TwDefine("Settings color='0 0 0' alpha=255 valueswidth=fit ");
	TwDefine("Settings refresh=0.5 contained=true");

	TwAddVarRW(m_UI, "Pos.", TW_TYPE_DIR3F, &cam_pos, "group=Camera");
	TwAddVarRW(m_UI, "Dir.", TW_TYPE_DIR3F, &cam_dir, "group=Camera");
	TwAddVarRW(m_UI, "Move Spd.", TW_TYPE_FLOAT, &cam_move_speed, "group=Camera");
	TwAddVarRW(m_UI, "Look Spd.", TW_TYPE_FLOAT, &cam_look_speed, "group=Camera");

	TwAddVarRO(m_UI, "FPS", TW_TYPE_FLOAT, &fps, "group=Renderer");
	TwAddVarRO(m_UI, "Frametime", TW_TYPE_FLOAT, &frame_time, "group=Renderer");
	
	glUseProgram(0);

	// set up profiling
	rmt_CreateGlobalInstance(&m_Remote);
	rmt_BindOpenGL();
}

void GLApp::Destroy()
{
	rmt_UnbindOpenGL();
	rmt_DestroyGlobalInstance(m_Remote);
	TwDeleteBar(m_UI);
	TwTerminate();
	m_Renderer->Destroy();
}

void GLApp::Run()
{
	while (!m_Input->ShouldQuit())
	{
		rmt_ScopedCPUSample(FrameUpdate, 0);

		// update delta and timer
		frames++;
		then = now;
		now = high_resolution_clock::now();
		time = now - then;
		delay += time.count();
		time_seconds += (float)time.count();
		dt = (float)std::chrono::duration_cast<std::chrono::milliseconds>(time).count();

		// we want to be able to read the framerate counter
		if (delay > delay_count)
		{
			std::stringstream title;
			//fps = frames / delay_count;

			frame_time = time.count() * 1000.0f;
			fps = 1000.0f / frame_time;
			/*title << "OpenGL: " << frames / delay_count;
			title << std::setprecision(3)
			<< std::fixed
			<< " FPS ("
			<< time.count() * 1000
			<< "ms) Elapsed " << (int)time_seconds << "s";*/
			frames = 0;
			delay = 0.0;
		}

		
		{
			rmt_ScopedCPUSample(InputUpdate, 0);
			m_Input->Update();
		}

		{

			rmt_ScopedCPUSample(ResourceUpdate, 0);
			m_Loader->UpdateResources();
		}

		rmt_BeginCPUSample(CameraUpdate, 0);
		if (m_Input->IsMouseCaptured())
		{
			// handle mouselook
			cam_yaw += m_Input->GetRelativeMouseX() * cam_look_speed * dt;
			cam_pitch += -m_Input->GetRelativeMouseY() * cam_look_speed * dt;
			cam_pitch = glm::clamp(cam_pitch, -89.0f, 89.0f);

			f32 rad_yaw = glm::radians(cam_yaw);
			f32 rad_pitch = glm::radians(cam_pitch);
			vec3 front_vec = vec3(cos(rad_pitch) * cos(rad_yaw), sin(rad_pitch), cos(rad_pitch) * sin(rad_yaw));
			front_vec = glm::normalize(front_vec);

			m_Camera->SetDirection(front_vec);
		}
		// handle camera movement

		if (m_Input->IsKeyDown(SDLK_w))
		{
			m_Camera->SetPosition(m_Camera->GetPosition() + (dt * cam_move_speed * m_Camera->GetDirection()));
		}
		if (m_Input->IsKeyDown(SDLK_s))
		{
			m_Camera->SetPosition(m_Camera->GetPosition() - (dt * cam_move_speed * m_Camera->GetDirection()));
		}

		if (m_Input->IsKeyDown(SDLK_a))
		{
			m_Camera->SetPosition(m_Camera->GetPosition() - (dt * cam_move_speed * glm::cross(m_Camera->GetDirection(), vec3(0.0f, 1.0f, 0.0f))));
		}

		if (m_Input->IsKeyDown(SDLK_d))
		{
			m_Camera->SetPosition(m_Camera->GetPosition() + (dt * cam_move_speed * glm::cross(m_Camera->GetDirection(), vec3(0.0f, 1.0f, 0.0f))));
		}
		m_Camera->Update();

		cam_pos = m_Camera->GetPosition();
		cam_dir = m_Camera->GetDirection();

		if (m_Input->IsMouseButtonJustPressed(MB_RIGHT))
		{
			m_Input->SetMouseCapture(true);
		}

		if (m_Input->IsMouseButtonJustReleased(MB_RIGHT))
		{
			m_Input->SetMouseCapture(false);
		}
		
		if (m_Input->IsKeyJustReleased(SDLK_u))
		{
			m_ShowUI = !m_ShowUI;
		}
		rmt_EndCPUSample();

		// update shader stuff
		//mat4 mvp = m_Camera->GetProjectionMatrix() * m_Camera->GetViewMatrix() * scene->GetModelMatrix();
		//mat4 mv = m_Camera->GetViewMatrix() * scene->GetModelMatrix();
		//mat4 m = scene->GetModelMatrix();
		//mat4 v = m_Camera->GetViewMatrix();
		m_CameraMatrices.M = m_Camera->GetModelMatrix();
		m_CameraMatrices.V = m_Camera->GetViewMatrix();
		m_CameraMatrices.P = m_Camera->GetProjectionMatrix();
		m_CameraMatrices.MV = m_CameraMatrices.V * m_CameraMatrices.M;
		m_CameraMatrices.MVP = m_CameraMatrices.P * m_CameraMatrices.MV;

		{
			rmt_ScopedOpenGLSample(RenderFrame);
			{
				rmt_ScopedOpenGLSample(BeginFrame);
				m_Renderer->BeginFrame();
			}

			{
				rmt_ScopedCPUSample(DrawFrame, 0);
				rmt_ScopedOpenGLSample(DrawFrame);
				RenderFrame();
			}

			if(m_ShowUI)
			{
				rmt_ScopedOpenGLSample(UIDraw);
				rmt_ScopedCPUSample(UIDraw, 0);
				TwDraw();
			}

			{
				rmt_ScopedOpenGLSample(SwapBuffers);
				m_Renderer->EndFrame(); 
			}
		}
	}
}

void GLApp::RenderFrame()
{
}

void GLApp::AddRenderPass(RenderPass* pass)
{
}

GLRenderer* GLApp::GetRenderer()
{
	return m_Renderer;
}

InputHandler* GLApp::GetInput()
{
	return m_Input;
}

ResourceLoader* GLApp::GetLoader()
{
	return m_Loader;
}

Camera* GLApp::GetCamera()
{
	return m_Camera;
}

TwBar* GLApp::GetUIBar()
{
	return m_UI;
}

f32 GLApp::GetDeltaTime()
{
	//return dt / 1000000000000.0f;
	return frame_time;
}


