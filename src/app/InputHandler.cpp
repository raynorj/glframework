#include "InputHandler.h"

InputHandler::InputHandler()
	: m_Quit(false),
	m_MouseCapture(false)
{
	SDL_PumpEvents();
	int num_keys = 0;
	auto key_state = SDL_GetKeyboardState(&num_keys);
	int x_pos = 0;
	int y_pos = 0;
	auto mouse_state = SDL_GetMouseState(&x_pos, &y_pos);

	for (int i = 0; i < num_keys; i++)
	{
		m_KeyState[i] = (key_state[i] != 0);
	}

	m_MouseButtonState[SDL_BUTTON_LEFT] = (mouse_state & SDL_BUTTON(SDL_BUTTON_LEFT)) == 1;
	m_MouseButtonState[SDL_BUTTON_RIGHT] = (mouse_state & SDL_BUTTON(SDL_BUTTON_RIGHT)) == 1;
	m_MouseButtonState[SDL_BUTTON_MIDDLE] = (mouse_state & SDL_BUTTON(SDL_BUTTON_MIDDLE)) == 1;
	m_RelativeMouseX = 0;
	m_RelativeMouseY = 0;
	m_PrevRelativeMouseX = 0;
	m_PrevRelativeMouseY = 0;
}

InputHandler::~InputHandler()
{
}

void InputHandler::SetMouseCapture(bool enabled)
{
	SDL_SetRelativeMouseMode((SDL_bool)enabled);
	m_MouseCapture = enabled;
	//SDL_ShowCursor(!enabled);
}

void InputHandler::Update()
{
	SDL_Event e;
	m_LastKeyState = m_KeyState;
	m_LastMouseButtonState = m_MouseButtonState;
	m_PrevRelativeMouseX = m_RelativeMouseX;
	m_PrevRelativeMouseY = m_RelativeMouseY;

	m_RelativeMouseX = 0;
	m_RelativeMouseY = 0;
	SDL_GetMouseState(&m_MouseX, &m_MouseY);
	bool handled = false;

	
	while (SDL_PollEvent(&e))
	{
		handled = TwEventSDL(&e, SDL_MAJOR_VERSION, SDL_MINOR_VERSION);
		switch (e.type)
		{
		case SDL_QUIT:
			m_Quit = true;
			break;
		case SDL_KEYUP:
			m_KeyState[e.key.keysym.sym] = false;
			break;
		case SDL_KEYDOWN:
			m_KeyState[e.key.keysym.sym] = true;
			break;
		case SDL_MOUSEBUTTONDOWN:
			m_MouseButtonState[e.button.button] = true;
			break;
		case SDL_MOUSEBUTTONUP:
			m_MouseButtonState[e.button.button] = false;
			break;
		case SDL_MOUSEMOTION:

			m_RelativeMouseX = e.motion.xrel;
			m_RelativeMouseY = e.motion.yrel;
			break;
		}
	}
}

bool InputHandler::IsKeyDown(SDL_Keycode key)
{
	return m_KeyState[key];
}

bool InputHandler::IsKeyUp(SDL_Keycode key)
{
	return !m_KeyState[key];
}

bool InputHandler::IsKeyReleased(SDL_Keycode key)
{

	return (!m_KeyState[key] && !m_LastKeyState[key]);
}

bool InputHandler::IsKeyJustReleased(SDL_Keycode key)
{
	return  (!m_KeyState[key] && m_LastKeyState[key]);
}

bool InputHandler::IsKeyPressed(SDL_Keycode key)
{
	return (m_KeyState[key] && m_LastKeyState[key]);
}

bool InputHandler::IsKeyJustPressed(SDL_Keycode key)
{
	return (m_KeyState[key] && !m_LastKeyState[key]);
}

bool InputHandler::IsMouseButtonDown(MouseButton button)
{
	return m_MouseButtonState[button];
}

bool InputHandler::IsMouseButtonUp(MouseButton button)
{
	return m_MouseButtonState[button];
}

bool InputHandler::IsMouseButtonReleased(MouseButton button)
{
	return (!m_MouseButtonState[button] && !m_LastMouseButtonState[button]);
}

bool InputHandler::IsMouseButtonJustReleased(MouseButton button)
{
	return (!m_MouseButtonState[button] && m_LastMouseButtonState[button]);
}

bool InputHandler::IsMouseButtonPressed(MouseButton button)
{
	return (m_MouseButtonState[button] && m_LastMouseButtonState[button]);
}

bool InputHandler::IsMouseButtonJustPressed(MouseButton button)
{
	return (m_MouseButtonState[button] && !m_LastMouseButtonState[button]);
}

f32 InputHandler::GetRelativeMouseX()
{
	return (float)m_RelativeMouseX;
}

f32 InputHandler::GetRelativeMouseY()
{
	return (float)m_RelativeMouseY;
}

s32 InputHandler::GetMouseX()
{
	return m_MouseX;
}

s32 InputHandler::GetMouseY()
{
	return m_MouseY;
}

bool InputHandler::IsMouseCaptured()
{
	return m_MouseCapture;
}

bool InputHandler::ShouldQuit()
{
	return m_Quit;
}
