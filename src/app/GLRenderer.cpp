#include "GLRenderer.h"

static void GLAPIENTRY DebugCallback(u32 source, u32 type, u32 id, u32 severity, s32 length, cchar message, const void* userParam)
{
	string msg_type = "";

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		msg_type = "Error";
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		msg_type = "Deprecated behavior";
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		msg_type = "Undefined behavior";
	case GL_DEBUG_TYPE_PORTABILITY:
		msg_type = "Portability issue";
	case GL_DEBUG_TYPE_PERFORMANCE:
		msg_type = "Performance issue";
	case GL_DEBUG_TYPE_MARKER:
		msg_type = "Stream annotation";
	case GL_DEBUG_TYPE_OTHER_ARB:
		msg_type = "Other";
	}

	string msg_source = "";
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		msg_source = "API";
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		msg_source = "Window system";
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		msg_source = "Shader compiler";
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		msg_source = "Third party";
	case GL_DEBUG_SOURCE_APPLICATION:
		msg_source = "Application";
	case GL_DEBUG_SOURCE_OTHER:
		msg_source = "Other";
	}

	string msg_severity = "";

	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		msg_severity = "High";
	case GL_DEBUG_SEVERITY_MEDIUM:
		msg_severity = "Medium";
	case GL_DEBUG_SEVERITY_LOW:
		msg_severity = "Low";
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		msg_severity = "Notification";
	}

	printf("\t\n[From %s {%s %s} 0x%X]\n%s\n", msg_source.c_str(), msg_severity.c_str(), msg_type.c_str(), id, message);
}

bool GLRenderer::Init(RendererOptions opts)
{
	// start up logging

	//std::freopen("output.log", "w", stdout);
	// Try to start SDL, then create window and context
	// Then set up singular shader program pipeline we'll use
		
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL startup failed: %s\n", SDL_GetError());
	}
	else
	{
		SDL_GL_LoadLibrary(NULL);
		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
		//SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);
		
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG | SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG);
	

		// set up window
		m_Window = SDL_CreateWindow(opts.WindowTitle.c_str(), SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			opts.WindowWidth,
			opts.WindowHeight,
			SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_BORDERLESS);

		if (m_Window == nullptr)
		{
			printf("SDL Window creation failed: %s\n", SDL_GetError());
			return false;
		}
		else
		{
			m_State = ContextState();
			m_Context = SDL_GL_CreateContext(m_Window);

			if (opts.WindowSize == ivec2(0, 0))
			{
				opts.WindowSize = ivec2(opts.WindowWidth, opts.WindowHeight);
			}


			m_Opts = opts;
			if (m_Context == nullptr)
			{
				printf("OpenGL context creation failed: %s\n", SDL_GetError());
				return false;
			}
			
			glewExperimental = true;
			glewInit();

			//SDL_GL_SetSwapInterval(0);
			Enable(OP_DEBUG_OUTPUT);
			Enable(OP_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(DebugCallback, nullptr);
			

			/*glDebugMessageControl(GL_DONT_CARE,
				GL_DONT_CARE,
				GL_DONT_CARE, 0, 0, GL_TRUE);//*/
			glDebugMessageControl(GL_DEBUG_SOURCE_OTHER,
				GL_DEBUG_TYPE_OTHER,
				GL_DEBUG_SEVERITY_NOTIFICATION, 0, 0, GL_FALSE);

			glDebugMessageControl(GL_DEBUG_SOURCE_OTHER,
				GL_DONT_CARE,
				GL_DEBUG_SEVERITY_NOTIFICATION, 0, 0, GL_FALSE);

			glDebugMessageControl(GL_DEBUG_SOURCE_OTHER,
				GL_DEBUG_TYPE_OTHER,
				GL_DONT_CARE, 0, 0, GL_FALSE);

				glDebugMessageControl(GL_DONT_CARE,
				GL_DEBUG_TYPE_OTHER,
				GL_DEBUG_SEVERITY_NOTIFICATION, 0, 0, GL_FALSE);

				glDebugMessageControl(GL_DEBUG_SOURCE_OTHER,
				GL_DEBUG_TYPE_OTHER,
				GL_DEBUG_SEVERITY_NOTIFICATION, 0, 0, GL_FALSE);

			int gl_ver[2] = { -1, -1 };
			glGetIntegerv(GL_MAJOR_VERSION, &gl_ver[0]);
			glGetIntegerv(GL_MINOR_VERSION, &gl_ver[1]);

			printf("Using OpenGL %i.%i\n", gl_ver[0], gl_ver[1]);
			

			//enable reverse Z
#if REVERSE_Z
			glClipControl(GL_LOWER_LEFT, GL_ZERO_TO_ONE);
			glClearDepth(0.0f);
			SetDepthFunc(CF_GREATER);
#else
			glClearDepth(1.0f);
			SetDepthFunc(CF_LESS);
#endif
			glClearColor(opts.ClearColor.r, opts.ClearColor.b, opts.ClearColor.g, 1.0f);
			
			m_ProgramPipeline = 0;
			glGenProgramPipelines(1, &m_ProgramPipeline);

			if (!m_ProgramPipeline)
			{
				printf("Couldn't create program pipeline.\n");
				return false;
			}
			glBindProgramPipeline(m_ProgramPipeline);
			
			m_Materials = std::vector<Material*>();
			SDL_GL_SetSwapInterval(opts.VSync);

			m_Quad = new FullScreenQuad();

			VertexArray* vao = new VertexArray();
			vao->Create();
			Bind(vao);
			m_Quad->Create(vao);
			Bind(0);
			m_Quad->SetPosition(vec3(0.0f));
			m_Quad->SetRotation(vec3(0.0f));
			m_Quad->SetScale(vec3(1.0f));

		}
	}
	return true;
}

void GLRenderer::Destroy()
{
	SDL_GL_DeleteContext(m_Context);
	SDL_DestroyWindow(m_Window);
	SDL_Quit();
}

void GLRenderer::SetWindowTitle(string title)
{
	SDL_SetWindowTitle(m_Window, title.c_str());
}

void GLRenderer::SetWindowFullscreenMode(u32 mode)
{
	if (m_State.ScreenMode != mode)
	{
		SDL_SetWindowFullscreen(m_Window, mode);
		m_State.ScreenMode = mode;
	}
	
}

void GLRenderer::SetWindowVSync(bool vsync)
{
	if (m_State.VSync != vsync)
	{
		SDL_GL_SetSwapInterval((vsync) ? 1 : 0);
		m_State.VSync = vsync;
	}
	
}

void GLRenderer::SetWindowsSize(ivec2 size)
{
	SDL_SetWindowSize(m_Window, size.x, size.y);
	m_Opts.WindowSize = size;
	m_Opts.WindowWidth = size.x;
	m_Opts.WindowHeight = size.y;
}

void GLRenderer::BeginFrame()
{
	u32 flags = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT;
	
	glViewport(0, 0, m_Opts.WindowWidth, m_Opts.WindowHeight);
	glClear(flags);
}


void GLRenderer::EndFrame()
{
	SDL_GL_SwapWindow(m_Window);
}

void GLRenderer::DrawArrays(VertexArray* array, PrimitiveType type)
{
	glDrawArrays(type, 0, array->GetSize() * 3);
}

void GLRenderer::DrawElements(u32 count, PrimitiveType type)
{
	glDrawElements(type, count, GL_UNSIGNED_INT, nullptr);
	//glDrawElementsInstancedBaseVertexBaseInstance(type, count, GL_UNSIGNED_INT, nullptr, count / 3, 0, 0);
}

void GLRenderer::SetFaceWinding(FaceWinding winding)
{
	if (m_State.Winding != winding)
	{
		glFrontFace(winding);
		m_State.Winding = winding;
	}
}

void GLRenderer::SetCullFace(PolygonFace face)
{
	if (m_State.CullFace != face)
	{
		glCullFace(face);
		m_State.CullFace = face;
	}
}

void GLRenderer::Enable(Operation op)
{
	auto itr = m_State.OpState.find(op);

	if (itr == m_State.OpState.end())
	{
		m_State.OpState.insert(std::make_pair(op, true));
		glEnable(op);
		return;
	}

	if (!itr->second)
	{
		glEnable(op);
		m_State.OpState[op] = true;
	}
}

void GLRenderer::Disable(Operation op)
{
	auto itr = m_State.OpState.find(op);

	if (itr == m_State.OpState.end())
	{
		m_State.OpState.insert(std::make_pair(op, false));
		glDisable(op);
		return;
	}

	if (itr->second)
	{
		glDisable(op);
		m_State.OpState[op] = false;
	}
}

void GLRenderer::Clear(BufferBits bits)
{
	glClear(bits);
}

void GLRenderer::Clear(TexturePtr texture, void* data)
{
	Bind(0, texture);
	glClearTexImage(texture->GetHandle(), 0, texture->GetInternalFormat(), texture->GetDataType(), data);
	Bind(0, 0);
}

void GLRenderer::SetBlendFunc(BlendFunc fSrc, BlendFunc fDst)
{
	if (m_State.SrcFunc != fSrc || m_State.DstFunc != fDst)
	{
		glBlendFunc(fSrc, fDst);
		m_State.SrcFunc = fSrc;
		m_State.DstFunc = fDst;
	}
}

void GLRenderer::SetBlendEquation(BlendEquation eq)
{
	if (m_State.BlendEq != eq)
	{
		glBlendEquation(eq);
		m_State.BlendEq = eq;
	}
}

void GLRenderer::SetDepthFunc(CompFunc func)
{
	if (m_State.DepthFunc != func)
	{
		glDepthFunc(func);
		m_State.DepthFunc = func;
	}
}

void GLRenderer::SetDepthWrite(bool enable)
{
	glDepthMask(enable);
}

void GLRenderer::SetPolygonMode(PolygonFace face, PolygonMode mode)
{
	if (face == PF_FRONT)
	{
		if (m_State.FrontMode == mode)
		{
			return;
		}
	}
	else if (face == PF_BACK)
	{
		if (m_State.BackMode == mode)
		{
			return;
		}
	}
	else if (face == PF_FRONT_AND_BACK)
	{
		if (m_State.BackMode == mode && m_State.FrontMode == mode)
		{
			return;
		}
	}

	if (face == PF_FRONT_AND_BACK)
	{
		m_State.BackMode = mode;
		m_State.FrontMode = mode;
	}
	else if (face == PF_BACK)
	{
		m_State.BackMode = mode;
	}
	else if (face == PF_FRONT)
	{
		m_State.FrontMode = mode;
	}
	glPolygonMode(face, mode);
}

void GLRenderer::Bind(VertexArray* vao)
{
	u32 handle = 0;
	if (vao)
	{
		handle = vao->GetHandle();
	}
	glBindVertexArray(handle);
}

void GLRenderer::Bind(u32 unit, TexturePtr texture)
{
	u32 handle = 0;
	if (texture != nullptr)
	{
		handle = texture->GetHandle();
	}
	glBindTextureUnit(unit, handle);
}

void GLRenderer::BindImage(u32 unit, TexturePtr texture, BufferAccess access)
{
	u32 handle = 0;
	TextureFormat format = TF_RGB16F;
	TextureType type = texture->GetType();

	if (!texture)
	{
		return;
	}

	handle = texture->GetHandle();
	format = texture->GetFormat();

	bool layered = false;
	if (type == TT_1D_ARRAY || type == TT_2D_ARRAY || type == TT_3D)
	{
		layered = true;
	}

	glBindImageTexture(unit, handle, 0, layered, 0, access, format);
}

void GLRenderer::BindImageLayer(u32 unit, TexturePtr texture, s32 layer, BufferAccess access)
{
	u32 handle = 0;
	TextureFormat format = TF_RGB16F;
	TextureType type = texture->GetType();

	if (!texture)
	{
		return;
	}

	handle = texture->GetHandle();
	format = texture->GetFormat();

	bool layered = false;
	if (type == TT_1D_ARRAY || type == TT_2D_ARRAY || type == TT_3D)
	{
		layered = true;
	}

	glBindImageTexture(unit, handle, layer, layered, 0, access, format);
}

void GLRenderer::SetActiveShader(ShaderPtr shader)
{
	u32 handle = 0;
	if (shader)
	{
		handle = shader->GetHandle();
	}
	glUseProgramStages(m_ProgramPipeline, shader->GetStageBit(), shader->GetHandle());
}

void GLRenderer::ResetShader(ShaderBits bit)
{
	glUseProgramStages(m_ProgramPipeline, bit, 0);
}

void GLRenderer::SetActiveFramebuffer(FramebufferPtr frame_buf)
{
	if (frame_buf)
	{
		frame_buf->Bind();
	}
	else
	{
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	
}

void GLRenderer::SetActiveRenderPass(RenderPass* pass)
{
	std::vector<TexturePtr> tex = pass->GetInput();
	FramebufferPtr fb = pass->GetOutput();
	ShaderPtr vs = pass->GetShaderVS();
	ShaderPtr ps = pass->GetShaderPS();


	SetActiveFramebuffer(fb);
	
	
	if (vs != nullptr)
	{
		SetActiveShader(vs);
	}

	if (ps != nullptr)
	{
		SetActiveShader(ps);
	}

	for (u32 i = 0; i < tex.size(); i++)
	{
		Bind(i, tex[i]);
	}
}

FramebufferPtr GLRenderer::CreateFramebuffer(ivec2 size, std::vector<TexturePtr> textures)
{
	FramebufferPtr fb = std::make_shared<Framebuffer>();
	fb->Create(size.x, size.y);
	for (int i = 0; i < textures.size(); i++)
	{
		u32 slot = 0;

		if (textures[i]->GetInternalFormat() != IF_DEPTH)
		{
			slot = i;
			
		}
		fb->BindTexture(slot, textures[i]);
	}
	fb->Finish();
	return fb;
}

TexturePtr GLRenderer::CreateRenderTargetTexture(ivec2 size, TextureFormat format, vec4 clear_color)
{
	InternalFormat internal = GetInternalFormat(format);
	TextureData type = GetDataType(format);

	std::vector<f32> clear;

	switch(internal)
	{
	case IF_R:
	case IF_DEPTH:
		clear.push_back(clear_color.r);
		break;
	case IF_RG:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		break;
	case IF_RGB:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		clear.push_back(clear_color.b);
		break;
	case IF_RGBA:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		clear.push_back(clear_color.b);
		clear.push_back(clear_color.a);
	}
	TexturePtr tex = std::make_shared<Texture>();
	tex->Create(TT_2D, format, internal, type, size.x, size.y);
	Clear(tex, &clear[0]);
	return tex;
}

RenderTarget2D* GLRenderer::CreateRenderTarget2D(ivec2 size, TextureFormat format, vec4 clear_color)
{
	RenderTarget2D* ret = new RenderTarget2D();
	TexturePtr tex = CreateRenderTargetTexture(size, format, clear_color);
	std::vector<TexturePtr> vec;
	vec.push_back(tex);

	FramebufferPtr fbuf = CreateFramebuffer(size, vec);
	ret->Create(tex, fbuf);
	return ret;
}

TexturePtr GLRenderer::CreateRenderTargetTexture(ivec3 size, TextureFormat format, s32 levels, vec4 clear_color)
{
	InternalFormat internal = GetInternalFormat(format);
	TextureData type = GetDataType(format);

	std::vector<f32> clear;

	switch (internal)
	{
	case IF_R:
	case IF_DEPTH:
		clear.push_back(clear_color.r);
		break;
	case IF_RG:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		break;
	case IF_RGB:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		clear.push_back(clear_color.b);
		break;
	case IF_RGBA:
		clear.push_back(clear_color.r);
		clear.push_back(clear_color.g);
		clear.push_back(clear_color.b);
		clear.push_back(clear_color.a);
	}
	TexturePtr tex = std::make_shared<Texture>();
	tex->Create(TT_3D, format, internal, type, size.x, size.y, size.z, levels);
	Clear(tex, &clear[0]);
	return tex;
}

RenderPass* GLRenderer::CreateRenderPass()
{
	return new RenderPass();
}

RendererOptions GLRenderer::GetOptions()
{
	return m_Opts;
}

void GLRenderer::AddMaterial(Material * m)
{
	m_Materials.push_back(m);
}

std::vector<Material*> GLRenderer::GetMaterials()
{
	return m_Materials;
}

void GLRenderer::RenderFSQuad()
{
	SetActiveShader(m_QuadVS);
	Bind(m_Quad->GetVAO());
	SetDepthWrite(false);
	Disable(OP_DEPTH_TEST);
	Disable(OP_CULL_FACE);
	SetPolygonMode(PF_FRONT_AND_BACK, PM_FILL);
	DrawElements(m_Quad->GetIndicies(), PT_TRI);
	SetDepthWrite(true);
	Enable(OP_DEPTH_TEST);
	Enable(OP_CULL_FACE);
}

void GLRenderer::SetQuadVS(ShaderPtr shader)
{
	m_QuadVS = shader;
}

void GLRenderer::SetQuadPS(ShaderPtr shader)
{
	m_QuadPS = shader;
}

ShaderPtr GLRenderer::GetQuadPS()
{
	return m_QuadPS;
}
