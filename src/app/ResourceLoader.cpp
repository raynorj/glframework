#include "ResourceLoader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

ResourceLoader::ResourceLoader(cchar root, GLRenderer* renderer)
{
	//IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF | IMG_INIT_WEBP);
	path root_dir = system_complete(root).parent_path();
	m_RootDir = root_dir;
	m_ExeDir = root_dir;
	m_Renderer = renderer;
}

ResourceLoader::~ResourceLoader()
{
	//IMG_Quit();
}

void ResourceLoader::UpdateResources()
{
	auto itr = m_ShaderCache.begin();
	auto times = m_ResourceWriteTime;
	for (auto itr = m_ShaderCache.begin(); itr != m_ShaderCache.end(); ++itr)
	{
		path p = GetResourcePath(itr->first);

		if (exists(p))
		{
			file_time_type curr = last_write_time(p);
			file_time_type prev;

			// make sure the file was found
			auto found_time = times.find(itr->first);
			if (found_time != times.end())
			{
				prev = found_time->second;
				if (curr > prev)
				{
					printf("File [%s] was updated, reloading.\n", itr->first.string().c_str());
					//times[itr->first] = curr;
					m_ShouldReload[itr->first] = true;
					LoadShader(itr->first); 
					m_ShouldReload[itr->first] = false;
				}
			}
			else
			{
				// no write time recorded, so file didn't exist when we checked for it
				// but does exist now, so load
				m_ShouldReload[itr->first] = true;
				LoadShader(itr->first);
				m_ShouldReload[itr->first] = false;
			}

			
		}
		else
		{
			// file still doesn't exist.
			// ignore
		}
		
	}
}

void ResourceLoader::SetRootDirectory(path dir)
{
	if (dir.empty() || !exists(dir))
	{
		printf("Directory '%s' is empty or does not exist.\n", dir.string().c_str());
		return;
	}

	path p = dir;
	if (p.filename() != "." && (p.string().end() != "\\" || p.string().end() != "/"))
	{
		p += path::preferred_separator;
	}
		
	p = p.make_preferred();
	m_RootDir = p;
}

path ResourceLoader::GetRootDirectory()
{
	return m_RootDir;
}

void ResourceLoader::SetRenderer(GLRenderer* renderer)
{
	m_Renderer = renderer;
}

GLRenderer* ResourceLoader::GetRenderer()
{
	return m_Renderer;
}

ShaderPtr ResourceLoader::LoadShader(path filename)
{
	
	ShaderPtr shader = nullptr;
	path full_path = GetResourcePath(filename);
	auto check = m_ShaderCache.find(filename);
	if (check != m_ShaderCache.end())
	{
		//printf("Shader already loaded.\n");
		if (!m_ShouldReload.find(filename)->second)
		{
			//printf("No need to reload shader.\n");
			return check->second;
		}
	}

	

	std::ifstream file;
	file.open(full_path.string().c_str(), std::ios_base::in);
	u32 program = 0;

	ShaderType type = ST_UNKNOWN;
	ShaderBits bit = SB_UNKNOWN;
	string extension = filename.extension().string();
	string str_type = "";
	string inc_string;
	path inc_file;
	if (extension == ".vs")
	{
		type = ST_VERTEX;
		bit = SB_VERTEX;
		str_type = "Vertex Shader";
	}
	else if (extension == ".ps")
	{
		type = ST_FRAGMENT;
		bit = SB_FRAGMENT;
		str_type = "Fragment Shader";
	}
	else if (extension == ".gs")
	{
		type = ST_GEOMETRY;
		bit = SB_GEOMETRY;
		str_type = "Geometry Shader";
	}
	else if (extension == ".tes")
	{
		type = ST_TESS_EVAL;
		bit = SB_TESS_EVAL;
		str_type = "Tessellation Evaluation Shader";
	}
	else if (extension == ".tcs")
	{
		type = ST_TESS_CTRL;
		bit = SB_TESS_CTRL;
		str_type = "Tessellation Control Shader";
	}
	else if (extension == ".cs")
	{
		type = ST_COMPUTE;
		bit = SB_COMPUTE;
		str_type = "Compute Shader";
	}

	if (!exists(full_path))
	{
		printf("Couldn't find shader: %s\n", full_path.string().c_str());

		shader = std::make_shared<Shader>(program, type, bit);
		m_ShaderCache.insert(std::make_pair(filename, shader));
		m_ShouldReload.insert(std::make_pair(filename, true));
		return shader;
	}

	if (file)
	{
		string src(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));
		file.close();
		path dir = full_path.parent_path();


		// get any includes
		// adapted from boost sample 
		// https://www.opengl.org/discussion_boards/showthread.php/169209-include-in-glsl
		std::regex inc_regex("^[ ]*#[ ]*include[ ]+[\"<](.*)[\">].*", std::regex_constants::ECMAScript);

		std::stringstream in, out;
		out << "";
		in << src;
		string line;
		u32 line_number = 1;
		std::smatch match;
		while (std::getline(in, line))
		{
			if (line_number == 1)
			{
				out << "#version 450 core" << std::endl;
				line_number++;
				continue;
			}
			else if (line.empty() || line == "")
			{
				line_number++;
				continue;
			}
			if (std::regex_search(line, match, inc_regex))
			{
				string m = (*match.begin()).str();
				
				m = m.substr(m.find_first_of("<"), m.find_first_of(">"));
				m = m.substr(1, m.length() - 2);
				path include_file = path(m);

				std::string include_string;
				path check_file = dir;
				check_file += path::preferred_separator;
				check_file += include_file;

				if(exists(check_file))
				{
					//printf("Including file [%s]\n", include_file.string().c_str());
					std::ifstream inc_file;
					inc_file.open(check_file.string().c_str(), std::ios_base::in);
					include_string = string(std::istreambuf_iterator<char>(inc_file), (std::istreambuf_iterator<char>()));
					inc_file.close();
					std::stringstream in2;
					in2 << include_string;
					string line2;
					//out << include_string;

					while (std::getline(in2, line2))
					{
						out << line2 << std::endl;
					}//*/
				}
				else
				{
					printf("%s (%i) : fatal error: cannot open include file %s\n", filename.string().c_str(), line_number, check_file.string().c_str());
				}
				
				//out << include_string << std::endl;
			}
			else 
			{
				//if (line_number > 1)
				//{
					//out << "#line " << line_number << " \"" << filename.string().c_str() << "\"" << std::endl;
				//}
				
				out << line << std::endl;
			}
			line_number++;
		}
		string out_str = out.str();
		

		std::ofstream ofile;
		path out_path = dir;
		out_path += path::preferred_separator;
		out_path += "processed";

		create_directory(out_path);

		out_path += path::preferred_separator;
		out_path += filename.filename();
		out_path += "_prec";
		ofile.open(out_path.string().c_str(), std::ios_base::out);
		ofile << out.str();
		ofile.close();

		// loading shader from memory grabs garbage for some reason, so output to temp file and load the file instead
		std::ifstream inc_file;
		inc_file.open(out_path.string().c_str(), std::ios_base::in);
		inc_string = string(std::istreambuf_iterator<char>(inc_file), (std::istreambuf_iterator<char>()));
		inc_file.close();
		const char* s = inc_string.c_str();
		bool compiled = true;
		if (type != ST_UNKNOWN)
		{
			program = glCreateShaderProgramv(type, 1, &s);
			
			s32 status = -1;
			glGetProgramiv(program, GL_LINK_STATUS, &status);
			if (!status)
			{
				s32 log_len = 0;
				glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_len);

				string log(log_len, ' ');
				glGetProgramInfoLog(program, log_len, &log_len, &log[0]);

				printf("Error compiling shader: %s\n%s\n", filename.string().c_str(), &log[0]);
				glDeleteProgram(program);
				program = 0;

				//if previous program worked, don't replace with 0.
				//if this is first load and the shader is broken, don't fail out, instead, return program 0
				//TODO: return empty shader?
				compiled = false;
			}
		}
		//delete s;

		// Asset already loaded, replace
		
		if (check != m_ShaderCache.end())
		{
			//printf("Replacing shader program.\n");
			ShaderPtr temp = check->second;
			if(program > 0)
			{ 
				temp->SetHandle(program);
			}
			// insert last write time
			auto time_check = m_ResourceWriteTime.find(filename);

			if (time_check != m_ResourceWriteTime.end())
			{

			}

			temp->ClearUniforms();
			m_ShaderCache[filename] = temp;
			m_ShouldReload.find(filename)->second = false;
			m_ResourceWriteTime[filename] = last_write_time(full_path);
		}
		else // loading for the first time
		{
			printf("Shader\t[%s]\nStage\t[%s]\n", filename.string().c_str(), str_type.c_str());
			shader = std::make_shared<Shader>(program, type, bit);
			m_ResourceWriteTime.insert(std::make_pair(filename, last_write_time(full_path)));
			m_ShaderCache.insert(std::make_pair(filename, shader));
			m_ShouldReload.insert(std::make_pair(filename, false));
		}
	}
	else //temp shader so things don't die
	{
		shader = std::make_shared<Shader>(program, type, bit);
		m_ShaderCache.insert(std::make_pair(filename, shader));
		m_ShouldReload.insert(std::make_pair(filename, false));
	}
	return shader;
}

MeshPtr ResourceLoader::LoadMesh(path filename)
{
	MeshPtr ret = nullptr;
	ret = std::make_shared<Mesh>();

	auto check = m_MeshCache.find(filename);

	// mesh already loaded, so grab it from cache
	if (check != m_MeshCache.end())
	{
		return check->second;
	}


	path full_path = GetResourcePath(filename);

	Assimp::Importer importer;
	const aiScene* scene = importer.ReadFile(full_path.string().c_str(),
		aiProcess_FindInstances |
		//aiProcess_OptimizeGraph |
		aiProcess_OptimizeMeshes |
		aiProcess_Triangulate |
		aiProcess_PreTransformVertices |
		aiProcess_JoinIdenticalVertices |
		//aiProcess_CalcTangentSpace | 
		aiProcess_GenSmoothNormals | 
		aiProcess_ImproveCacheLocality
		//aiProcess_LimitBoneWeights | 
		//aiProcess_FindDegenerates |
		/*aiProcess_FindInvalidData*/);

	if (!scene)
	{
		printf("Couldn't load mesh: %s\n", filename.string().c_str());
		return ret;
	}
	printf("Loading mesh [%s]\n", filename.string().c_str());
	// todo: materials and textures.
	// also, implement texture and mesh cache
	u32 mat_count = scene->mNumMaterials;

	

	DrawElementsIndirectCommandList list;
	u32 vertex_count = 0;
	u32 index_count = 0;
	// make sure we get all submeshes too
	for (u32 i = 0; i < scene->mNumMeshes; i++)
	{

		const aiMesh* mesh = scene->mMeshes[i];
		//bool is_tri = (bool)(mesh->mPrimitiveTypes & aiPrimitiveType_TRIANGLE);
		VertexArray* vao = new VertexArray();
		vao->Create();
		m_Renderer->Bind(vao);
		// index buffer is easy enough
		Buffer* ib = new Buffer();
		std::vector<u32> ibv;
		
		ib->Create(BT_INDEX, BA_READ, BU_NONE);

		DrawElementsIndirectCommand cmd;
		cmd.BaseVertex = vertex_count;
		cmd.FirstIndex = 0;
		cmd.InstanceCount = 1;
		cmd.BaseInstance = 0;
		for (u32 j = 0; j < mesh->mNumFaces; j++)
		{
			aiFace face = mesh->mFaces[j];
			/**if (face.mNumIndices == 3)
			{
				ibv.push_back(face.mIndices[0]);
				ibv.push_back(face.mIndices[1]);
				ibv.push_back(face.mIndices[2]);

			}//*/
			/**/for (u32 k = 0; k < face.mNumIndices; k++)
			{
				ibv.push_back(face.mIndices[k]);
			}//*/

		}
		// now for the vertex buffers
		Buffer* pos = nullptr;
		Buffer* norm = nullptr;
		Buffer* tex = nullptr;
		Buffer* col = nullptr;

		std::vector<vec3> verts;
		std::vector<vec3> normals;
		std::vector<vec2> uvs;
		std::vector<vec4> colors;
		vec3 half(0.0f);
		vec3 min_pos;
		vec3 max_pos;


		// vertex position
		if (mesh->HasPositions())
		{
			pos = new Buffer();
			pos->Create(BT_VERTEX, BA_READ, BU_NONE);
		}

		// vertex normals
		if (mesh->HasNormals())
		{
			norm = new Buffer();
			norm->Create(BT_VERTEX, BA_READ, BU_NONE);
		}

		// texture coordinates
		if (mesh->HasTextureCoords(0))
		{
			tex = new Buffer();
			tex->Create(BT_VERTEX, BA_READ, BU_NONE);
		}

		// vertex colors
		if (mesh->HasVertexColors(0))
		{
			col = new Buffer();
			col->Create(BT_VERTEX, BA_READ, BU_NONE);
		}

		// fill buffers
		for (u32  j = 0; j <= mesh->mNumVertices; j++)
		{
			if (mesh->HasPositions())
			{
				aiVector3D pos = mesh->mVertices[j];
				vec3 p = vec3(pos.x, pos.y, pos.z);
				verts.push_back(p);


				min_pos.x = glm::min(p.x, min_pos.x);
				min_pos.y = glm::min(p.y, min_pos.y);
				min_pos.z = glm::min(p.z, min_pos.z);

				max_pos.x = glm::max(p.x, max_pos.x);
				max_pos.y = glm::max(p.y, max_pos.y);
				max_pos.z = glm::max(p.z, max_pos.z);
			}

			if (mesh->HasNormals())
			{
				aiVector3D nor = mesh->mNormals[j];
				vec3 normal = vec3(nor.x, nor.y, nor.z);
				normals.push_back(normal);
			}

			if (mesh->HasTextureCoords(0))
			{
				aiVector3D uv = mesh->mTextureCoords[0][j];
				vec2 coord = vec2(uv.x, uv.y);
				uvs.push_back(coord);
			}
			else
			{
				vec2 coord = vec2();
				uvs.push_back(coord);
			}

			if (mesh->HasVertexColors(0))
			{
				aiColor4D* color = mesh->mColors[j];
				vec4 c = vec4(color->r, color->g, color->b, color->a);
				colors.push_back(c);

			}
		}

		vertex_count += mesh->mNumVertices;
		cmd.VertexCount = mesh->mNumVertices;
		

		if (mesh->HasPositions())
		{
			pos->SetData(&(verts)[0], verts.size(), sizeof(vec3), sizeof(float));
			verts.erase(verts.begin(), verts.end());
			vao->BindBuffer(pos, 0);
		}

		if (mesh->HasNormals())
		{
			norm->SetData(&(normals)[0], normals.size(), sizeof(vec3), sizeof(float));
			normals.erase(normals.begin(), normals.end());
			vao->BindBuffer(norm, 2);
			
		}

		if (mesh->HasTextureCoords(0))
		{
			tex->SetData(&(uvs)[0], uvs.size(), sizeof(vec2), sizeof(float));
			uvs.erase(uvs.begin(), uvs.end());
			vao->BindBuffer(tex, 1);
		}

		if (mesh->HasVertexColors(0))
		{
			col->SetData(&(colors)[0], colors.size(), sizeof(vec4), sizeof(float));
			colors.erase(colors.begin(), colors.end());
			vao->BindBuffer(col, 3);
		}
		
		

		ib->SetData(&(ibv)[0], ibv.size(), sizeof(u32), sizeof(u32));
		ibv.erase(ibv.begin(), ibv.end());
		vao->BindBuffer(ib, 0);

		// grab material
		u32 mat_id = m_Renderer->GetMaterials().size();
		Material* m = new Material(mat_id);
		u32 index = mesh->mMaterialIndex;
		aiMaterial* mat = scene->mMaterials[index];

		path p = filename.parent_path();
		path diffuse = GetAssimpTexture(mat, aiTextureType_DIFFUSE, p, m);
		path specular = GetAssimpTexture(mat, aiTextureType_AMBIENT, p, m);
		path mask = GetAssimpTexture(mat, aiTextureType_OPACITY, p, m);
		path normal = GetAssimpTexture(mat, aiTextureType_HEIGHT, p, m);
		path roughness = GetAssimpTexture(mat, aiTextureType_SHININESS, p, m);

		if (!diffuse.empty())
		{
			//printf("Getting diffuse.\n");
			m->SetDiffuseMap(LoadTexture(diffuse));
		}

		if (!specular.empty())
		{
			//printf("Getting specular.\n");
			m->SetMetalnessMap(LoadTexture(specular));
		}

		if (!mask.empty())
		{
			//printf("Getting opacity.\n");
			m->SetOpacityMap(LoadTexture(mask));
		}

		if (!normal.empty())
		{
			//printf("Getting height.\n");
			m->SetNormalMap(LoadTexture(normal));
		}

		if (!roughness.empty())
		{
			//printf("Getting roughness.\n");
			m->SetRoughnessMap(LoadTexture(roughness));
		}
		
		// check to see if we've already created this material
		std::vector<Material*> mats = m_Renderer->GetMaterials();

		auto itr = mats.begin();
		Material* copy = nullptr;
		while (itr != mats.end())
		{
			Material* m2 = *itr;

			// check diffuse
			TexturePtr t1 = m->GetDiffuseMap();
			TexturePtr t2 = m2->GetDiffuseMap();

			if (!IsDuplicate(t1, t2))
			{
				itr++;
				continue;
			}


			// metal
			t1 = m->GetMetalnessMap();
			t2 = m2->GetMetalnessMap();

			if (!IsDuplicate(t1, t2))
			{
				itr++;
				continue;
			}

			// opacity
			t1 = m->GetOpacityMap();
			t2 = m2->GetOpacityMap();

			if (!IsDuplicate(t1, t2))
			{
				itr++;
				continue;
			}

			// normal
			t1 = m->GetNormalMap();
			t2 = m2->GetNormalMap();

			if (!IsDuplicate(t1, t2))
			{
				itr++;
				continue;
			}

			// roughness
			t1 = m->GetRoughnessMap();
			t2 = m2->GetRoughnessMap();

			if (!IsDuplicate(t1, t2))
			{
				itr++;
				continue;
			}

			printf("Found copy in material#%s\n", m2->GetID());
			copy = m2;
			break;
			itr++;
		}

		if (copy != nullptr)
		{
			m = copy;
		}
		else
		{
			m_Renderer->AddMaterial(m);
		}//*/

		MeshPart* part = new MeshPart();
		part->SetData(vao, ib);
		part->SetMaterial(m);
		ret->AddPart(part);
	}

	for (u32 i = 0; i < ret->GetPartCount(); i++)
	{
		//printf("Part #%d, material #%d\n", i, ret->GetPart(i)->GetMaterial()->GetID());
	}
	m_MeshCache.insert(std::make_pair(filename, ret));
	return ret;
}

TexturePtr ResourceLoader::LoadTexture(path filename)
{

	// TODO: pick g or SDL_image
	TexturePtr ret = nullptr;
	
	auto check = m_TextureCache.find(filename);

	if (check != m_TextureCache.end())
	{
		//printf("\t[%s already loaded, pulling from cache.]\n", filename.string().c_str());
		return check->second;
	}

	s32 width = 0;
	s32 height = 0;
	s32 depth = 1;
	s32 comp = 0;
	//SDL_Surface* temp = IMG_Load(GetResourcePath(filename).string().c_str());
	unsigned char *data = stbi_load(GetResourcePath(filename).string().c_str(), &width, &height, &comp, 0);
	//printf("\t\nTexture\t[%s]\n", filename.string().c_str());
	//if (!temp)
	if(!data)
	{
		printf("Failed to load texture file: %s\n", filename.string().c_str());
		return ret;
	}

	// get texture size 
	//width = temp->w;
	//height = temp->h;
	depth = 1;
	// get texture type
	// only caring about 1/2d textures when loading from disk

	//printf("Size\t[%ix%ix%i]\n", width, height, depth);
	TextureType type;
	if(width == 1 || height == 1)
	{
		type = TT_1D;
	}
	else
	{
		type = TT_2D;
	}

	// get format conversions for creating GL texture
	//SDL_PixelFormat* fmt_in = temp->format;
	TextureFormat fmt = TF_RGB8;
	InternalFormat inter_fmt = IF_RGB;
	string fmt_str = "";

	//if (fmt_in->BytesPerPixel == 4)
	if(comp == 4)
	{
		// RGBA
		fmt = TF_RGBA8;
		//if (fmt_in->Rmask == 0x000000ff)
		{
			inter_fmt = IF_RGBA;
			fmt_str = "RGBA";
		}
		/*else
		{
			inter_fmt = IF_BGRA;
			fmt_str = "BGRA";
		}*/
		
	}
	//else if (fmt_in->BytesPerPixel == 3)
	else if(comp == 3)
	{
		//RGB
		fmt = TF_RGB8;

		//if (fmt_in->Rmask == 0x000000ff)
		//{
			inter_fmt = IF_RGB;
			fmt_str = "RGB";
		/*}
		else
		{
			inter_fmt = IF_BGR;
			fmt_str = "BGR";
		}*/
	}
	else if (comp == 2)
	{
		//Greyscale (assumed)
		// Not going to ever use gifs/jpgs given the option, so don't care.
		fmt = TF_RG8;
		inter_fmt = IF_RG;
		fmt_str = "GREYSCALE ALPHA";

	}
	//else if (fmt_in->BytesPerPixel == 1)
	else if (comp == 1)
	{
		//Greyscale (assumed)
		// Not going to ever use gifs/jpgs given the option, so don't care.
		fmt = TF_R8;
		inter_fmt = IF_R;
		fmt_str = "GREYSCALE";
		
	}
	//printf("Format\t[%s]\n", fmt_str.c_str());

	/*if (SDL_MUSTLOCK(temp))
	{
		SDL_LockSurface(temp);
	}

	void* data = temp->pixels;

	if (SDL_MUSTLOCK(temp))
	{
		SDL_UnlockSurface(temp);
	}*/

	
	ret = std::make_shared<Texture>();
	ret->Create(type, fmt, inter_fmt, TD_UBYTE, width, height, depth, 3);
	m_Renderer->Bind(0, ret);
	ret->SetMagFilter(FM_LINEAR);
	ret->SetMinFilter(FM_LINEAR_MIPMAP_LINEAR);
	ret->SetWrapR(WM_REPEAT);
	ret->SetWrapS(WM_REPEAT);
	//ret->SetMaxMipLevel(9);
	//ret->SetBaseMipLevel(5);
	ret->SetData(data);
	stbi_image_free(data);
	
	ret->GenerateMipMaps();
	m_Renderer->Bind(0, nullptr);


	m_TextureCache.insert(std::make_pair(filename, ret));
	return ret;
}

JSON ResourceLoader::LoadJSON(path filename)
{
	JSON ret = JSON();
	if (!exists(GetResourcePath(filename)))
	{
		printf("JSON [%s] failed to load\n", filename.string().c_str());
	}
	else
	{
		std::ifstream file;
		file.open(GetResourcePath(filename).string().c_str(), std::ios_base::in);
		string src(std::istreambuf_iterator<char>(file), (std::istreambuf_iterator<char>()));

		//printf("JSON [%s] loaded", filename.string().c_str());
		
		ret = JSON::parse(src.c_str());
	}
	return ret;
}

Material* ResourceLoader::LoadMaterial(path filename)
{
	/*
		Materials are stored in json files, which reference albedo, roughness, normal/bump, and roughness textures.
		Material files also can specify which shaders to use for all shader stages except compute
	*/

	JSON j = LoadJSON(filename);

	if (j.empty())
	{
		printf("Material couldn't load.\n");
		return nullptr;
	}
	u32 mat_id = m_Renderer->GetMaterials().size();
	Material* mat = new Material(mat_id);
	path base_path = filename.parent_path();

	path texture_path = base_path;
	texture_path += path::preferred_separator;

	path diff = texture_path;
	diff += j["albedo_map"].get<string>();
	mat->SetDiffuseMap(LoadTexture(diff));

	path norm = texture_path;
	norm += j["normal_map"].get<string>();
	mat->SetNormalMap(LoadTexture(norm));


	bool transparent = j["has_transparency"].get<bool>();
	if (transparent)
	{
		path opacity = texture_path;
		opacity += j["opacity_map"].get<string>();
		mat->SetOpacityMap(LoadTexture(opacity));
	}

	path rough = texture_path;
	rough += j["roughness_map"].get<string>();
	mat->SetRoughnessMap(LoadTexture(rough));

	path metal = texture_path;
	metal += j["metalness_map"].get<string>();
	mat->SetMetalnessMap(LoadTexture(metal));

	return mat;
}
path ResourceLoader::GetResourcePath(path file)
{
	path temp = m_RootDir;
	temp += path::preferred_separator;
	temp += file;
	return temp;
}

path ResourceLoader::GetAssimpTexture(aiMaterial* imp_mat, aiTextureType type, path dir, Material * mat)
{
	aiString texture_path;
	if (imp_mat->GetTexture(type, 0, &texture_path) == AI_SUCCESS)
	{
		path p = dir;
		p += p.preferred_separator;
		p += path(texture_path.C_Str());
		return p;
	}
	return path();
}

bool ResourceLoader::IsDuplicate(TexturePtr t1, TexturePtr t2)
{
	if (t1 && t2)
	{
		if (t1->GetHandle() == t2->GetHandle())
		{
			return true;
		}
	}
	return false;
}
