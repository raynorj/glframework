#include "Shader.h"

Shader::Shader(u32 handle, ShaderType type, ShaderBits bit)
{
	SetHandle(handle);
	m_Type = type;
	m_StageBit = bit;
}

Shader::~Shader()
{
	glDeleteProgram(m_ShaderProgram);
}

void Shader::SetHandle(u32 shader)
{
	m_ShaderProgram = shader;
}

u32 Shader::GetHandle()
{
	return m_ShaderProgram;
}

ShaderType Shader::GetType()
{
	return m_Type;
}

ShaderBits Shader::GetStageBit()
{
	return m_StageBit;
}

void Shader::SetUniform(s32 location, f32 value)
{
	glProgramUniform1fv(m_ShaderProgram, location, 1, &value);
}

void Shader::SetUniform(s32 location, vec2 value)
{
	glProgramUniform2fv(m_ShaderProgram, location, 1, &value[0]);
}

void Shader::SetUniform(s32 location, vec3 value)
{
	glProgramUniform3fv(m_ShaderProgram, location, 1, &value[0]);
}

void Shader::SetUniform(s32 location, vec4 value)
{
	glProgramUniform4fv(m_ShaderProgram, location, 1, &value[0]);
}

void Shader::SetUniform(s32 location, mat4 value)
{
	glProgramUniformMatrix4fv(m_ShaderProgram, location, 1, false, &value[0][0]);
}

void Shader::SetUniform(s32 location, s32 value)
{
	glProgramUniform1iv(m_ShaderProgram, location, 1, &value);
}

void Shader::SetUniform(s32 location, u32 value)
{
	glProgramUniform1uiv(m_ShaderProgram, location, 1, &value);
}



void Shader::SetUniformByName(string name, f32 value)
{
	glProgramUniform1fv(m_ShaderProgram, GetUniformLocation(name), 1, &value);
}

void Shader::SetUniformByName(string name, vec2 value)
{
	glProgramUniform2fv(m_ShaderProgram, GetUniformLocation(name), 1, &value[0]);
}

void Shader::SetUniformByName(string name, vec3 value)
{
	glProgramUniform3fv(m_ShaderProgram, GetUniformLocation(name), 1, &value[0]);
}

void Shader::SetUniformByName(string name, vec4 value)
{
	glProgramUniform4fv(m_ShaderProgram, GetUniformLocation(name), 1, &value[0]);
}

void Shader::SetUniformByName(string name, mat4 value)
{
	glProgramUniformMatrix4fv(m_ShaderProgram, GetUniformLocation(name), 1, false, &value[0][0]);
}

void Shader::SetUniformByName(string name, s32 value)
{
	glProgramUniform1iv(m_ShaderProgram, GetUniformLocation(name), 1, &value);
}

void Shader::SetUniformByName(string name, u32 value)
{
	glProgramUniform1uiv(m_ShaderProgram, GetUniformLocation(name), 1, &value);
}



void Shader::Dispatch(u32 size_x, u32 size_y, u32 size_z)
{
	if (m_Type != ST_COMPUTE)
		return;

	glDispatchCompute(size_x, size_y, size_z);
}

void Shader::ClearUniforms()
{
	m_UniformLocations.clear();
}

s32 Shader::GetUniformLocation(string name)
{
	s32 loc = -1;
	auto itr = m_UniformLocations.find(name);
	if (itr == m_UniformLocations.end())
	{
		loc = glGetUniformLocation(m_ShaderProgram, name.c_str());

		if (loc != -1)
		{
			m_UniformLocations.insert(std::make_pair(name, loc));
			printf("Found shader uniform %s at location %i.\n", name.c_str(), loc);
		}
		
	}
	else
	{
		loc = itr->second;
	}

	return loc;
}
