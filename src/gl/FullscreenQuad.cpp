#include "FullscreenQuad.h"

FullScreenQuad::FullScreenQuad()
{
}

FullScreenQuad::~FullScreenQuad()
{
}

void FullScreenQuad::Create(VertexArray* VAO)
{
	std::vector<vec3> verts;
	verts.push_back(vec3(1.0f, 1.0f, 0.0f));//upper right
	verts.push_back(vec3(1.0f, -1.0f, 0.0f));//lower right
	verts.push_back(vec3(-1.0f, -1.0f, 0.0f));//lower left
	verts.push_back(vec3(-1.0f, 1.0f, 0.0f));//upper left

	std::vector<vec2> uvs;
	uvs.push_back(vec2(1.0f, 0.0f));
	uvs.push_back(vec2(1.0f, 1.0f));
	uvs.push_back(vec2(0.0f, 1.0f));
	uvs.push_back(vec2(0.0f, 0.0f));


	std::vector<u32> indices;
	indices.push_back(0);
	indices.push_back(3);
	indices.push_back(1);
	indices.push_back(1);
	indices.push_back(3);
	indices.push_back(2);

	Buffer* vbuf = new Buffer();
	vbuf->Create(BT_VERTEX, BA_RW, BU_RWABLE);
	vbuf->SetData(&(verts)[0], verts.size(), sizeof(vec3), sizeof(float));

	Buffer* uvbuf = new Buffer();
	uvbuf->Create(BT_VERTEX, BA_RW, BU_RWABLE);
	uvbuf->SetData(&(uvs)[0], uvs.size(), sizeof(vec2), sizeof(float));

	Buffer* ibuf = new Buffer();
	ibuf->Create(BT_INDEX, BA_RW, BU_RWABLE);
	ibuf->SetData(&(indices)[0], indices.size(), sizeof(u32), sizeof(u32));

	verts.erase(verts.begin(), verts.end());
	uvs.erase(uvs.begin(), uvs.end());
	indices.erase(indices.begin(), indices.end());


	VAO->BindBuffer(vbuf, 0);
	VAO->BindBuffer(uvbuf, 1);
	VAO->BindBuffer(ibuf, 0);

	MeshPart* part = new MeshPart();
	part->SetData(VAO, ibuf);
	AddPart(part);
}

VertexArray* FullScreenQuad::GetVAO()
{
	return GetPart(0)->GetVertexArray();
}

u32 FullScreenQuad::GetIndicies()
{
	return 6;
}
