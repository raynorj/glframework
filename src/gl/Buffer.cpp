#include "Buffer.h"

Buffer::Buffer(): 
	m_Size(0),
	m_ElementSize(0)
{
	m_IsMapped = false;
}

Buffer::~Buffer()
{
	Delete();
}

void Buffer::Create(BufferType type, BufferAccess access_flag, BufferUsageBit usage)
{
	//printf("Begin create buffer.\n");
	glCreateBuffers(1, &m_BufferID);
	//printf("End create buffer.\n");
	m_Type = type;
	m_Access = access_flag;
	m_Usage = usage;

}

void Buffer::Delete()
{
	glDeleteBuffers(1, &m_BufferID);
}

void Buffer::SetData(void* data, u32 size, u32 el_size, u32 type_size)
{
	//printf("Begin buffer storage.\n");
	glNamedBufferStorage(m_BufferID, el_size * size, data, m_Usage);
	//printf("End buffer storage.\n");
	//void* newdata = GetData();
	//newdata = data;
	if (m_IsMapped)
	{
		//glUnmapNamedBuffer(m_BufferID);
		m_IsMapped = false;
	}
	
	m_Size = size;
	m_ElementSize = el_size;
	m_TypeSize = type_size;
}

void* Buffer::GetData()
{
	m_IsMapped = true;
	return glMapNamedBuffer(m_BufferID, m_Access);
}

u32 Buffer::GetSize()
{
	return m_Size;
}

void Buffer::SetElementSize(u32 size)
{
	m_ElementSize = size;
}

u32 Buffer::GetElementSize()
{
	return m_ElementSize;
}

u32 Buffer::GetTypeSize()
{
	return m_TypeSize;
}

u32 Buffer::GetHandle()
{
	return m_BufferID;
}

BufferType Buffer::GetType()
{
	return m_Type;
}
