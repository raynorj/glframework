#include "VertexArray.h"

VertexArray::VertexArray() :
	m_IsBound(false),
	m_IndexBuffer(nullptr),
	m_ArrayID(0)
{
}

VertexArray::~VertexArray()
{
}

void VertexArray::Create()
{
	glCreateVertexArrays(1, &m_ArrayID);
}

void VertexArray::Destroy()
{
	glDeleteVertexArrays(1, &m_ArrayID);
}

void VertexArray::BindBuffer(Buffer* buffer, u32 binding)
{
	//printf("Element size is: %i, number of elements is: %i.\n", buffer->GetElementSize(), buffer->GetSize());
	//printf("Size should be: %i.\n", buffer->GetElementSize() / buffer->GetSize());
	//printf("float size: %i\n vec3 size: %i\n", sizeof(float), sizeof(vec3));
	//printf("Type size is: %i\n", buffer->GetTypeSize());
	//printf("Begin VAO bind.\n");
	switch (buffer->GetType())
	{
	case BT_VERTEX:
		//printf("AttribBinding\n");
		glVertexArrayAttribBinding(m_ArrayID, binding, binding);
		//printf("EnableAttrib\n");
		glEnableVertexArrayAttrib(m_ArrayID, binding);
		//printf("AttribFormat\n");
		glVertexArrayAttribFormat(m_ArrayID, binding, buffer->GetElementSize() / buffer->GetTypeSize(), GL_FLOAT, GL_FALSE, 0);
		//printf("VertexBuffer\n");
		glVertexArrayVertexBuffer(m_ArrayID, binding, buffer->GetHandle(), 0, buffer->GetElementSize());
		//glEnableVertexAttribArray(binding);
		//glVertexAttribPointer(binding, buffer->GetElementSize() / buffer->GetTypeSize(), GL_FLOAT, false, buffer->GetElementSize(), (void*)0);
		break;
	case BT_INDEX:
		glVertexArrayElementBuffer(m_ArrayID, buffer->GetHandle());
		m_IndexBuffer = buffer;
		break;
	default:
		printf("Invalid buffer type used to bind.\n");
		break;
	}
	//printf("End VAO bind.\n");
}

u32 VertexArray::GetSize()
{
	if (m_IndexBuffer)
	{
		return m_IndexBuffer->GetSize();
	}
	return 0;
}

u32 VertexArray::GetHandle()
{
	return m_ArrayID;
}
