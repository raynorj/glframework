#include "Light.h"

Light::Light()
{
	m_Color = vec3(1.0f, 1.0f, 1.0f);
	m_Intensity = 1000.0f;
}

Light::~Light()
{
}

void Light::SetColor(vec3 color)
{
	m_Color = color;
}

vec3 Light::GetColor()
{
	return m_Color;
}

void Light::SetIntensity(f32 i)
{
	m_Intensity = i;
}

float Light::GetIntensity()
{
	return m_Intensity;
}

void Light::SetCastsShadow(bool cast)
{
	m_CastsShadow = true;
}

bool Light::GetCastsShadow()
{
	return m_CastsShadow;
}
