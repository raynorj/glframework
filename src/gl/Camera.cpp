#include "Camera.h"

Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::Update()
{
	Entity::Update();

	
	float f = 1.0f / tan(m_FOV / 2.0f);
	
	m_ProjectionMatrix = glm::perspective(m_FOV, m_AspectRatio, m_NearPlane, m_FarPlane);

#if REVERSE_Z
	float p22 = m_ProjectionMatrix[2][2];
	float p32 = m_ProjectionMatrix[3][2];
	m_ProjectionMatrix[2][2] = -p22 - 1.0f;
	m_ProjectionMatrix[3][2] = -p32;
	/*m_ProjectionMatrix = glm::mat4(
		f / m_AspectRatio, 0.0f, 0.0f, 0.0f,
		0.0f, f, 0.0f, 0.0f,
		0.0f, 0.0f, 0.0f, -1.0f,
		0.0f, 0.0f,2*m_NearPlane, 0.0f);//*/
#endif
	m_ViewMatrix = glm::lookAt(m_Position, m_Position + m_Direction, vec3(0.0f, 1.0f, 0.0f));
}

void Camera::SetAspectRatio(f32 ratio)
{
	m_AspectRatio = ratio;
	m_Dirty = true;
}

void Camera::SetNearPlane(f32 near)
{
	m_NearPlane = near;
	m_Dirty = true;
}

void Camera::SetFarPlane(f32 far)
{
	m_FarPlane = far;
	m_Dirty = true;
}

void Camera::SetFOV(f32 fov)
{
	m_FOV = fov;
	m_Dirty = true;
}

void Camera::SetDirection(vec3 dir)
{
	m_Direction = dir;
	m_Dirty = true;
}

f32 Camera::GetAspectRatio()
{
	return m_AspectRatio;
}

f32 Camera::GetNearPlane()
{
	return m_NearPlane;
}

f32 Camera::GetFarPlane()
{
	return m_FarPlane;
}

f32 Camera::GetFOV()
{
	return m_FOV;
}

vec3 Camera::GetDirection()
{
	return m_Direction;
}

mat4 Camera::GetViewMatrix()
{
	if (m_Dirty)
	{
		Update();
	}
	return m_ViewMatrix;
}

mat4 Camera::GetProjectionMatrix()
{
	if (m_Dirty)
	{
		Update();
	}
	return m_ProjectionMatrix;
}
