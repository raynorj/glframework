#include "TextureTypes.h"

InternalFormat GetInternalFormat(TextureFormat format)
{
	InternalFormat f;

	switch (format)
	{
	case TF_DEPTH_16:
	case TF_DEPTH_24:
	case TF_DEPTH_32:
		f = IF_DEPTH;
		break;

	case TF_R8:
	case TF_R8I:
	case TF_R8UI:
	case TF_R16:
	case TF_R16I:
	case TF_R16UI:
	case TF_R16F:
	case TF_R32I:
	case TF_R32UI:
	case TF_R32F:
		f = IF_R;
		break;
		// 2 channel
	case TF_RG8:
	case TF_RG8I:
	case TF_RG8UI:
	case TF_RG16:
	case TF_RG16I:
	case TF_RG16UI:
	case TF_RG16F:
	case TF_RG32I:
	case TF_RG32UI:
	case TF_RG32F:
		f = IF_RG;
		break;
	case TF_RGB8:
	case TF_RGB8I:
	case TF_RGB8UI:
	case TF_RGB16:
	case TF_RGB16I:
	case TF_RGB16UI:
	case TF_RGB16F:
	case TF_RGB32I:
	case TF_RGB32UI:
	case TF_RGB32F:
		f = IF_RGB;
		break;
	case TF_RGBA8:
	case TF_RGBA8I:
	case TF_RGBA8UI:
	case TF_RGBA16:
	case TF_RGBA16I:
	case TF_RGBA16UI:
	case TF_RGBA16F:
	case TF_RGBA32I:
	case TF_RGBA32UI:
	case TF_RGBA32F:
		f = IF_RGBA;
		break;
	}

	return f;
}

TextureData GetDataType(TextureFormat format)
{
	TextureData d;

	switch (format)
	{
	case TF_DEPTH_16:
	case TF_DEPTH_24:
		d = TD_UINT;
		break;

	case TF_R8:
	case TF_R8I:
	case TF_R16:
	case TF_R16I:
	case TF_R32I:
	case TF_RG8:
	case TF_RG8I:
	case TF_RG16:
	case TF_RG16I:
	case TF_RG32I:
	case TF_RGB8:
	case TF_RGB8I:
	case TF_RGB16:
	case TF_RGB16I:
	case TF_RGBA8:
	case TF_RGBA8I:
	case TF_RGBA16:
	case TF_RGBA16I:
	case TF_RGB32I:
	case TF_RGBA32I:
		d = TD_INT;
		break;

	case TF_R8UI:
	case TF_R16UI:
	case TF_R32UI:
	case TF_RG8UI:
	case TF_RG16UI:
	case TF_RG32UI:
	case TF_RGB8UI:
	case TF_RGB16UI:
	case TF_RGBA8UI:
	case TF_RGBA16UI:
	case TF_RGB32UI:
	case TF_RGBA32UI:
		d = TD_UINT;
		break;

	case TF_R16F:
	case TF_R32F:
	case TF_RG16F:
	case TF_RG32F:
	case TF_RGB16F:
	case TF_RGB32F:
	case TF_RGBA16F:
	case TF_RGBA32F:
	case TF_DEPTH_32:
		d = TD_FLOAT;
		break;
	}
	return d;
}
