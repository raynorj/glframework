#include "MeshPart.h"

MeshPart::MeshPart()
{
}

MeshPart::~MeshPart()
{
}

void MeshPart::SetData(VertexArray* vao, Buffer* ibo)
{
	m_VAO = vao;
	m_IndexBuffer = ibo;
}

void MeshPart::SetMaterial(Material * mat)
{
	m_Material = mat;
}

Material * MeshPart::GetMaterial()
{
	return m_Material;
}

VertexArray* MeshPart::GetVertexArray()
{
	return m_VAO;
}
