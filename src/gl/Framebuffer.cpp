#include "Framebuffer.h"

Framebuffer::Framebuffer()
{

}

Framebuffer::~Framebuffer()
{
}

void Framebuffer::Create(u32 width, u32 height)
{
	glCreateFramebuffers(1, &m_BufferID);
	m_Width = width;
	m_Height = height;
}

void Framebuffer::Destroy()
{
	glDeleteFramebuffers(1, &m_BufferID);
}

void Framebuffer::Finish()
{
	if (m_Binds.size() > 0)
	{
		glNamedFramebufferDrawBuffers(m_BufferID, m_Binds.size(), &m_Binds[0]);
	}
	else
	{
		//printf("No color buffer added to FBO, disabling color reads and writes.\n");
		glNamedFramebufferDrawBuffer(m_BufferID, GL_NONE);
		glNamedFramebufferReadBuffer(m_BufferID, GL_NONE);
	}
}

void Framebuffer::Bind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_BufferID);
	
}

void Framebuffer::Unbind()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Framebuffer::Clear(vec4 color)
{
	Bind();
	glClearColor(color.r, color.g, color.b, color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	Unbind();
}

void Framebuffer::BeginDraw()
{
	int index = 0;
#if REVERSE_Z
	static f32* depth_f = new f32(0.0f);
#else
	static f32* depth_f = new f32(1.0f);
#endif

	static f32* color_f = new f32(0.0f);
	static s32* color_i = new s32(0);
	static u32* color_ui = new u32(0);
	for (u32 i = 0; i < m_Targets.size(); i++)
	{
		TexturePtr target = m_Targets[i];
		switch (target->GetInternalFormat())
		{
		case IF_DEPTH:
			glClearNamedFramebufferfv(m_BufferID, GL_DEPTH, 0, depth_f);
			break;
		case IF_RGB:
		case IF_RGBA:
			switch (target->GetDataType())
			{
			case TD_FLOAT:
				glClearNamedFramebufferfv(m_BufferID, GL_COLOR, index, color_f);
				break;
			case TD_INT:
			case TD_BYTE:
				glClearNamedFramebufferiv(m_BufferID, GL_COLOR, index, color_i);
				break;
			case TD_UINT:
			case TD_UBYTE:
			case TD_USHORT:
				glClearNamedFramebufferuiv(m_BufferID, GL_COLOR, index, color_ui);
				break;
			}
			index++;
			break;

		}
	}
}

void Framebuffer::BindTexture(u32 slot, TexturePtr texture)
{
	u32 attach = GL_COLOR_ATTACHMENT0 + slot;

	
	if (texture->GetFormat() == TF_DEPTH_16 ||
		texture->GetFormat() == TF_DEPTH_24 ||
		texture->GetFormat() == TF_DEPTH_32)
	{
		attach = GL_DEPTH_ATTACHMENT;
	}

	//glNamedFramebufferTextureLayer(m_BufferID, attach, texture->GetHandle(), 0, 0);
	glNamedFramebufferTexture(m_BufferID, attach, texture->GetHandle(), 0);
	

	// don't bind the depth attachment
	if (attach != GL_DEPTH_ATTACHMENT)
	{
		m_Binds.push_back(attach);
	}
	m_Targets.push_back(texture);
	
}
