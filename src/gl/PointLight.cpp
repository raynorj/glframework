#include "PointLight.h"

PointLight::PointLight()
{
	m_Data = PointLightData();
	SetPosition(m_Data.Position);
	SetRadius(m_Data.Radius);
}

void PointLight::SetPosition(vec3 pos)
{
	m_Position = pos;
}

vec3 PointLight::GetPosition()
{
	return m_Position;
}

void PointLight::SetRadius(f32 r)
{
	m_Radius = r;
}

float PointLight::GetRadius()
{
	return m_Radius;
}

PointLightData PointLight::GetData()
{
	return m_Data;
}

Camera* PointLight::GetCamera()
{
	return nullptr;
}
