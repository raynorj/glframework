#include "RenderTarget2D.h"

RenderTarget2D::RenderTarget2D()
{
	m_Texture = nullptr;
	m_FBO = nullptr;
}

RenderTarget2D::~RenderTarget2D()
{
}

void RenderTarget2D::Create(TexturePtr tex, FramebufferPtr fbo)
{
	if (!tex)
	{
		printf("Cannot create RenderTarget2D: Texture handle is invalid.\n");
	}

	if (!fbo)
	{
		printf("Cannot create RenderTarget2D: Framebuffer handle is invalid.\n");
	}

	if (!tex || !fbo)
	{
		return;
	}

	m_Texture = tex;
	m_FBO = fbo;
}

void RenderTarget2D::Destroy()
{
	m_FBO->Destroy();
	m_Texture->Destroy();
}

u32 RenderTarget2D::GetTextureID()
{
	return m_Texture->GetHandle();
}

FramebufferPtr RenderTarget2D::GetFBO()
{
	return m_FBO;
}

TexturePtr RenderTarget2D::GetTexturePtr()
{
	return m_Texture;
}
