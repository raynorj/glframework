#include "..\..\include\gl\SpotLight.h"

SpotLight::SpotLight()
{
	m_Data = SpotLightData();
	SetPosition(m_Data.Position);
	SetDirection(m_Data.Direction);
	SetRadius(m_Data.Radius);
	SetConeAngle(m_Data.ConeAngle);
	m_Camera = new Camera();
	m_Camera->SetAspectRatio(1.0f);
	m_Camera->SetNearPlane(0.1f);
}

void SpotLight::SetPosition(vec3 pos)
{
	m_Position = pos;
}

vec3 SpotLight::GetPosition()
{
	return m_Position;
}

void SpotLight::SetDirection(vec3 dir)
{
	m_Direction = dir;
}

vec3 SpotLight::GetDirection()
{
	return m_Direction;
}

void SpotLight::SetRadius(f32 r)
{
	m_Radius = r;
}

f32 SpotLight::GetRadius()
{
	return m_Radius;
}

void SpotLight::SetConeAngle(f32 angle)
{
	m_ConeAngle = angle;
}

f32 SpotLight::GetConeAngle()
{
	return m_ConeAngle;
}

SpotLightData SpotLight::GetData()
{
	return m_Data;
}

Camera* SpotLight::GetCamera()
{
	m_Camera->SetPosition(m_Position);
	m_Camera->SetDirection(m_Direction);
	m_Camera->SetFarPlane(m_Radius);
	m_Camera->SetFOV(glm::radians(m_ConeAngle * 2));

	m_Camera->Update();
	return m_Camera;
}
