#include "Material.h"

Material::Material(u32 id)
{
	m_ID = id;
}

Material::~Material()
{
}

u32 Material::GetID()
{
	return m_ID;
}

void Material::SetUseDiffuseMap(bool use)
{
	m_UseDiffuseMap = use;
}

bool Material::GetUseDiffuseMap()
{
	return m_UseDiffuseMap;
}

void Material::SetDiffuseMap(TexturePtr map)
{
	m_DiffuseMap = map;
}

TexturePtr Material::GetDiffuseMap()
{
	return m_DiffuseMap;
}

void Material::SetDiffuse(vec3 color)
{
	m_DiffuseColor = color;
}

vec3 Material::GetDiffuse()
{
	return m_DiffuseColor;
}

void Material::SetUseOpacityMap(bool use)
{
	m_UseOpacityMap = use;
}

bool Material::GetUseOpacityMap()
{
	return m_UseOpacityMap;
}

void Material::SetOpacityMap(TexturePtr map)
{
	m_OpacityMap = map;
}

TexturePtr Material::GetOpacityMap()
{
	return m_OpacityMap;
}

void Material::SetOpacity(f32 opacity)
{
	m_Opacity = opacity;
}

f32 Material::GetOpacity()
{
	return m_Opacity;
}

void Material::SetUseNormalMap(bool use)
{
	m_UseNormalMap = use;
}

bool Material::GetUseNormalMap()
{
	return m_UseNormalMap;
}

void Material::SetNormalMap(TexturePtr map)
{
	m_NormalMap = map;
}

TexturePtr Material::GetNormalMap()
{
	return m_NormalMap;
}

void Material::SetUseRoughnessMap(bool use)
{
	m_UseRoughnessMap = use;
}

bool Material::GetUseRoughnessMap()
{
	return m_UseRoughnessMap;
}

void Material::SetRoughnessMap(TexturePtr map)
{
	m_RoughnessMap = map;
}

TexturePtr Material::GetRoughnessMap()
{
	return m_RoughnessMap;
}

void Material::SetRoughness(f32 rough)
{
	m_Roughness = rough;
}

f32 Material::GetRoughness()
{
	return m_Roughness;
}

void Material::SetUseMetalnessMap(bool use)
{
	m_UseMetalnessMap = use;
}

bool Material::GetUseMetalnessMap()
{
	return m_UseMetalnessMap;
}

void Material::SetMetalnessMap(TexturePtr map)
{
	m_MetalnessMap = map;
}

TexturePtr Material::GetMetalnessMap()
{
	return m_MetalnessMap;
}

void Material::SetMetalness(f32 metal)
{
	m_Metalness = metal;
}

f32 Material::GetMetalness()
{
	return m_Metalness;
}


