#include "..\..\include\gl\OrthoCamera.h"

void OrthoCamera::Update()
{
	Entity::Update();

	m_ProjectionMatrix = glm::ortho(m_Extents.x, m_Extents.y, m_Extents.z, m_Extents.w, m_NearPlane, m_FarPlane);

	vec3 up = vec3(0.0f, 1.0f, 0.0f);
	if (m_Direction == vec3(0.0f, 1.0f, 0.0f) || m_Direction == vec3(0.0f, -1.0f, 0.0f))
	{
		up = vec3(m_Direction.x, m_Direction.z, m_Direction.y);
	}
	m_ViewMatrix = glm::lookAt(m_Position, m_Position + m_Direction, up);
	
}

void OrthoCamera::SetExtents(vec4 ext)
{
	m_Extents = ext;
}
