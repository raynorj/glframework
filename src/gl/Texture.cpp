#include "Texture.h"

Texture::Texture()
	:m_Depth(-1),
	m_Height(-1),
	m_Width(-1),
	m_Type(TT_1D)
{
}

Texture::~Texture()
{
	Destroy();
}

void Texture::Create(TextureType type, TextureFormat format, InternalFormat internal_format, TextureData data_type, s32 width, s32 height, s32 depth, s32 levels)
{
	m_Type = type;
	m_Format = format;
	m_InternalFormat = internal_format;
	m_DataType = data_type;
	m_Width = width;
	m_Height = height;
	m_Depth = depth;

	glCreateTextures(type, 1, &m_TextureID);

	switch (m_Type)
	{
	case TT_1D:
		glTextureStorage1D(m_TextureID, levels, m_Format, m_Width);
		break;
	case TT_2D:
		glTextureStorage2D(m_TextureID, levels, m_Format, m_Width, m_Height);
		break;
	case TT_2D_ARRAY:
		glTextureStorage3D(m_TextureID, levels, m_Format, m_Width, m_Height, m_Depth);
		break;
	case TT_3D:
		glTextureStorage3D(m_TextureID, levels, m_Format, m_Width, m_Height, m_Depth);
		break;
	case TT_CUBE:
		glTextureStorage2D(m_TextureID, levels, m_Format, m_Width, m_Height);
		break;
	}
}

void Texture::Destroy()
{
	glDeleteTextures(1, &m_TextureID);
}

s32 Texture::GetWidth()
{
	return m_Width;
}

s32 Texture::GetHeight()
{
	return m_Height;
}

s32 Texture::GetDepth()
{
	return m_Depth;
}

TextureType Texture::GetType()
{
	return m_Type;
}

TextureFormat Texture::GetFormat()
{
	return m_Format;
}

InternalFormat Texture::GetInternalFormat()
{
	return m_InternalFormat;
}

TextureData Texture::GetDataType()
{
	return m_DataType;
}

u32 Texture::GetHandle()
{
	return m_TextureID;
}

void Texture::GenerateMipMaps()
{
	//glTextureParameterf(m_TextureID, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
	glGenerateTextureMipmap(m_TextureID);
}

void Texture::SetData(void* data)
{
	
	switch (m_Type)
	{
	case TT_1D:
		glTextureSubImage1D(m_TextureID, 0, 0, m_Width, m_InternalFormat, m_DataType, data);
		break;
	case TT_2D:
		glTextureSubImage2D(m_TextureID, 0, 0, 0, m_Width, m_Height, m_InternalFormat, m_DataType, data);
		break;
	case TT_2D_ARRAY:
		glTextureSubImage3D(m_TextureID, 0, 0, 0, 0, m_Width, m_Height, m_Depth, m_InternalFormat, m_DataType, data);
		break;
	case TT_3D:
		glTextureSubImage3D(m_TextureID, 0, 0, 0, 0, m_Width, m_Height, m_Depth, m_InternalFormat, m_DataType, data);
		break;

	}
	glBindTexture(m_Type, m_TextureID);
	glBindTexture(m_Type, 0);
}

void Texture::SetFaceData(u32 face, void* data)
{
	if (m_Type == TT_CUBE)
	{
		glTextureSubImage3D(m_TextureID, 0, 0, 0, face, m_Width, m_Height, m_Depth, m_InternalFormat, m_DataType, data);
	}
	else if (m_Type == TT_2D_ARRAY)
	{
		glTextureSubImage3D(m_TextureID, 0, 0, 0, face, m_Width, m_Height, 1, m_InternalFormat, m_DataType, data);
	}
}

void Texture::SetMinFilter(FilterMode filter)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_MIN_FILTER, filter);
}

void Texture::SetMagFilter(FilterMode filter)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_MAG_FILTER, filter);
}

void Texture::SetWrapS(WrapMode wrap)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_S, wrap);
}

void Texture::SetWrapT(WrapMode wrap)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_T, wrap);
}

void Texture::SetWrapR(WrapMode wrap)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_WRAP_R, wrap);
}

void Texture::SetBaseMipLevel(u32 level)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_BASE_LEVEL, level);
}

void Texture::SetMaxMipLevel(u32 level)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_MAX_LEVEL, level);
}

void Texture::SetCompareMode(CompareMode mode)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_COMPARE_MODE, mode);
}

void Texture::SetCompareFunc(CompFunc func)
{
	glTextureParameteri(m_TextureID, GL_TEXTURE_COMPARE_FUNC, func);
}
