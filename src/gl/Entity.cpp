#include "Entity.h"

Entity::Entity()
	: m_Position(0.0f),
	m_Rotation(0.0f),
	m_Scale(0.0f),
	m_ModelMatrix(0.0f)
{

}

Entity::~Entity()
{

}
void Entity::Update()
{
	if (!m_Dirty)
	{
		return;
	}

	mat4 trans_mat = glm::translate(m_Position);
	mat4 scale_mat = glm::scale(m_Scale);
	mat4 rot_mat = glm::yawPitchRoll<float>(m_Rotation.x, 
											m_Rotation.y, 
											m_Rotation.z);
	
	m_ModelMatrix = trans_mat * rot_mat * scale_mat;
	
	vec4 min = vec4(m_AABB.MinPoint, 1.0f);
	vec4 max = vec4(m_AABB.MaxPoint, 1.0f);

	AABB temp;
	min = m_ModelMatrix * min;
	max = m_ModelMatrix * max;


	m_Dirty = false;
}
void Entity::SetPosition(vec3 pos)
{
	m_Position = pos;
	m_Dirty = true;
}

void Entity::SetX(f32 x)
{
	m_Position.x = x;
	m_Dirty = true;
}

void Entity::SetY(f32 y)
{
	m_Position.y = y;
	m_Dirty = true;
}

void Entity::SetZ(f32 z)
{
	m_Position.z = z;
	m_Dirty = true;
}

vec3 Entity::GetPosition()
{
	return m_Position;
}

f32 Entity::GetX()
{
	return m_Position.x;
}

f32 Entity::GetY()
{
	return m_Position.y;
}

f32 Entity::GetZ()
{
	return m_Position.z;
}

void Entity::SetRotation(vec3 rot)
{
	m_Rotation = rot;
	m_Dirty = true;
}

void Entity::SetYaw(f32 x)
{
	m_Rotation.x = x;
	m_Dirty = true;
}

void Entity::SetPitch(f32 y)
{
	m_Rotation.y = y;
	m_Dirty = true;
}

void Entity::SetRoll(f32 z)
{
	m_Rotation.z = z;
	m_Dirty = true;
}

vec3 Entity::GetRotation()
{
	return m_Rotation;
}

f32 Entity::GetYaw()
{
	return m_Rotation.x;
}

f32 Entity::GetPitch()
{
	return m_Rotation.y;
}

f32 Entity::GetRoll()
{
	return m_Rotation.z;
}

void Entity::SetScale(vec3 scale)
{
	m_Scale = scale;
	m_Dirty = true;
}

void Entity::SetScaleX(f32 x)
{
	m_Scale.x = x;
	m_Dirty = true;
}

void Entity::SetScaleY(f32 y)
{
	m_Scale.y = y;
	m_Dirty = true;
}

void Entity::SetScaleZ(f32 z)
{
	m_Scale.z = z;
	m_Dirty = true;
}

vec3 Entity::GetScale()
{
	return m_Scale;
}

f32 Entity::GetScaleX()
{
	return m_Scale.x;
}

f32 Entity::GetScaleY()
{
	return m_Scale.y;
}

f32 Entity::GetScaleZ()
{
	return m_Scale.z;
}

mat4 Entity::GetModelMatrix()
{
	if (m_Dirty)
	{
		Update();
	}
	return m_ModelMatrix;
}

AABB Entity::GetAABB()
{
	if (m_Dirty)
	{
		Update();
	}

	return m_AABB;
}

