#include "RenderPass.h"

RenderPass::RenderPass()
{
	m_ShaderPS = nullptr;
	m_ShaderVS = nullptr;
	m_Output = nullptr;
}

RenderPass::~RenderPass()
{

}

void RenderPass::AddInput(TexturePtr in)
{
	m_Input.push_back(in);
}

void RenderPass::SetOutput(FramebufferPtr out)
{
	m_Output = out;
}

void RenderPass::SetShaderVS(ShaderPtr shader)
{
	m_ShaderVS = shader;
}
void RenderPass::SetShaderPS(ShaderPtr shader)
{
	m_ShaderPS = shader;
}

std::vector<TexturePtr> RenderPass::GetInput()
{
	return m_Input;
}

FramebufferPtr RenderPass::GetOutput()
{
	return m_Output;
}

ShaderPtr RenderPass::GetShaderVS()
{
	return m_ShaderVS;
}

ShaderPtr RenderPass::GetShaderPS()
{
	return m_ShaderPS;
}

