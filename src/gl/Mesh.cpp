#include "Mesh.h"

Mesh::Mesh()
{

}

Mesh::~Mesh()
{

}

void Mesh::AddPart(MeshPart* part)
{
	m_Parts.push_back(part);

	std::sort(m_Parts.begin(), m_Parts.end(), 
	[](MeshPart* l, MeshPart* r) -> bool
	{
		return l->GetMaterial()->GetID() < r->GetMaterial()->GetID();
	});
}

u32 Mesh::GetPartCount()
{
	return m_Parts.size();
}

MeshPart* Mesh::GetPart(u32 index)
{
	if (index >= m_Parts.size())
	{
		return nullptr;
	}
	return m_Parts.at(index);
}
