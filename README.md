# README #

### Rendering Framework ###

This framework was written to simplify OpenGL development for personal projects

### Setup ###

The project requires the following dependencies, and has only been tested on Windows.

* SDL2 v2.0.5
* GLM v0.9.8.4
* GLEW v2.0.0
* Assimp v3.3.1

### Configuration ###

The project uses the CMake build system. After pointing CMake to the source and build directories, set the DEPS variable to the location of the folder containing any dependencies. 

Dependencies should be, in general, in the following format (note removal of version numbers)

/dependency/

|--/include/

|--/lib/

The only current exception to this is GLM, which instead places /glm-0.9.8.4/glm/glm (The second level containing a CMakeLists file) into the /glm/ folder.
